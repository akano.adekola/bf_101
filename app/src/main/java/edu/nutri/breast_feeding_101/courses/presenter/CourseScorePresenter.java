package edu.nutri.breast_feeding_101.courses.presenter;

import android.view.View;
import android.widget.TextView;

import edu.nutri.breast_feeding_101.common.util.CircleDisplay;
import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.util.ValidatorUtil;

/**
 * Created by ribads on 6/2/18.
 */
public class CourseScorePresenter {

    public CourseScorePresenter(View view, int postAssessmentScore) {
        TextView recommendation_text = view.findViewById(R.id.recommendation_text);
        TextView status_text = view.findViewById(R.id.status_text);
        CircleDisplay mCircleDisplay = view.findViewById(R.id.circleDisplay);

        mCircleDisplay.setAnimDuration(postAssessmentScore, 4000);
        mCircleDisplay.setValueWidthPercent(55f);
//        mCircleDisplay.setFormatDigits(1);
        mCircleDisplay.setDimAlpha(80);
//        mCircleDisplay.setSelectionListener(this);
        mCircleDisplay.setTouchEnabled(true);
        mCircleDisplay.setUnit("%");
        mCircleDisplay.setStepSize(0.5f);
        mCircleDisplay.showValue(postAssessmentScore, 100f, true);

        if(ValidatorUtil.isCoursePasses(postAssessmentScore)){
            status_text.setText( "Pass!");
            status_text.setTextColor(view.getResources().getColor(android.R.color.holo_green_light));
            recommendation_text.setText( "Proceed to the next course");
        }
        else{
            status_text.setText("Fail");
            status_text.setTextColor(view.getResources().getColor(android.R.color.holo_red_light));
            recommendation_text.setText( "Retake the course to proceed");
        }
    }
}
