package edu.nutri.breast_feeding_101.courses.interactor;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.CountDownTimer;
import android.view.View;
import android.view.animation.Animation;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

import java.util.Random;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.courses.presenter.CourseAssessmentPresenter;
import edu.nutri.breast_feeding_101.database.models.QuizLeaderBoard;

/**
 * Created by ribads on 6/2/18.
 */
public class CourseAssessmentInteractor {

    private CourseAssessmentPresenter presenter;
    private CountDownTimer cdt;

    public CourseAssessmentInteractor(View view, View.OnClickListener onClickListener) {
        presenter = new CourseAssessmentPresenter(view, onClickListener);
    }

    private void startBlinkingText() {
        presenter.startBlinkingText();
    }

    private void setTimeUpText() {
        presenter.setTimeUptext();
    }

    private void setCdttext(long millisUntilFinished) {
        presenter.setCdtText(millisUntilFinished);
    }

    public void clearAnimations() {
        presenter.clearAnimation();
    }

    public void setQuestionOptions(int question_no, int random) {
        presenter.setQuestionOptions(question_no, random);
    }

    public String getAnswer(int random) {
        return presenter.getAnswer(random);
    }

    public void shakeQuestionLayout() {
        presenter.shakeQuestionLayout();
    }

    public String getSelectedOption(int id) {
        String[] options = presenter.getOptions();
        switch (id) {
            case R.id.option_1_btn:
                return options[0];
            case R.id.option_2_btn:
                return options[1];
            case R.id.option_3_btn:
                return options[2];
            case R.id.option_4_btn:
                return options[3];
        }
        return "";
    }

    public int getRandomNumber(int courseNo) {
        Random ran = new Random();

        switch (courseNo) {
            case 0:
                return ran.nextInt(57);
            case 1:
                return ran.nextInt(9);
            case 2:
                return ran.nextInt((19 + 1) - 10) + 10;
            case 3:
                return ran.nextInt((27 + 1) - 20) + 20;
            case 4:
                return ran.nextInt((37 + 1) - 28) + 28;
            case 5:
                return ran.nextInt((47 + 1) - 38) + 38;
            case 6:
                return ran.nextInt((57 + 1) - 48) + 48;
        }
        return 0;
    }

    public void setButtonClickedBg(View view) {
        Drawable drawableNormal = view.getBackground();
        Drawable drawablePressed = view.getBackground().getConstantState().newDrawable();
        drawablePressed.mutate();
        drawablePressed.setColorFilter(Color.parseColor("#ff0000"), PorterDuff.Mode.SRC_ATOP);

        StateListDrawable listDrawable = new StateListDrawable();
        listDrawable.addState(new int[]{android.R.attr.state_pressed}, drawablePressed);
        listDrawable.addState(new int[]{}, drawableNormal);
        view.setBackground(listDrawable);
    }
    public void initTimer() {
        cdt = new CountDownTimer(26000, 1000) {

            public void onTick(long millisUntilFinished) {
                if ((millisUntilFinished / 1000) <= 6)
                    startBlinkingText();

                setCdttext(millisUntilFinished);

                if ((millisUntilFinished / 1000) == 1)
                    setTimeUpText();
            }
            public void onFinish() {
            }
        };
    }

    public void cancelTimer() {
        cdt.cancel();
    }

    public void startTimer() {
        cdt.start();
    }

    public void hideQuizLayout() {
        presenter.hideQuizLayout();
    }

    public void removeLivesImage() {
        presenter.removeLivesImage();
    }

    public void setScoreText(int score) {
        presenter.setScoreText(score);
    }

}
