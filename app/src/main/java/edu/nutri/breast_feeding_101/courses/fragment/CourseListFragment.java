package edu.nutri.breast_feeding_101.courses.fragment;


import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import edu.nutri.breast_feeding_101.courses.view.CourseAssessmentActivity;
import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.listener.ClickListener;
import edu.nutri.breast_feeding_101.common.util.NotificationUtil;
import edu.nutri.breast_feeding_101.common.view.BaseFragment;
import edu.nutri.breast_feeding_101.dashboard.adapter.CourseListAdapter;
import edu.nutri.breast_feeding_101.database.models.CoursesScores;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class CourseListFragment extends BaseFragment implements ClickListener<Integer> {


    public CourseListFragment() {
        // Required empty public constructor
    }

    RecyclerView recyclerView;
    CourseListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return getFragmentView(inflater, R.layout.fragment_course_list, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view) {
        String[] courseTitles = getResources().getStringArray(R.array.course_titles);
        String[] courseDescription = getResources().getStringArray(R.array.course_description);
//        int[] courseIcons = getResources().getIntArray(R.array.nav_drawer_icons);
        final TypedArray courseIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

        recyclerView = view.findViewById(R.id.course_list_recycler);
        adapter = new CourseListAdapter();
        adapter.setData(courseTitles, courseDescription, courseIcons);
        adapter.setListener(this);
//        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        // TODO: 6/3/18 Lock Course
        checkIfCourse1isCompleted();
    }

    private void checkIfCourse1isCompleted() {
//        FirebaseDatabase database = FirebaseDatabase.getInstance();
//        DatabaseReference databaseReference = database.getReference().child("Test");
//        databaseReference.child(CoursesScores.tableName).equalTo(1L, "CourseNo").equalTo(userId, "userId").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                if (dataSnapshot.exists()){
//                    NotificationUtil.showToast(getActivity(), "exists");
//                }
//                else
//                    NotificationUtil.showToast(getActivity(), "not exists");
//            }
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
    }

    @Override
    public void onClick(Integer position) {
        Bundle bundle = new Bundle();
        bundle.putInt("courseNo", position+1);
        bundle.putBoolean("isPreAssessment", true);
//        LauncherUtil.launchClass(getActivity(), CoursePreAssessment.class, bundle);
        Intent intent = new Intent(getActivity(), CourseAssessmentActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, 321);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
        }else if(resultCode == RESULT_CANCELED){
        }
    }
}
