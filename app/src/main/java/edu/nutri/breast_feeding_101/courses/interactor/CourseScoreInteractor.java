package edu.nutri.breast_feeding_101.courses.interactor;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

import edu.nutri.breast_feeding_101.common.util.IdGenerator;
import edu.nutri.breast_feeding_101.common.util.NotificationUtil;
import edu.nutri.breast_feeding_101.courses.presenter.CourseScorePresenter;
import edu.nutri.breast_feeding_101.database.models.CoursesScores;
import edu.nutri.breast_feeding_101.database.models.Statistics;
import edu.nutri.breast_feeding_101.database.models.User;

/**
 * Created by ribads on 6/2/18.
 */
public class CourseScoreInteractor {

    public CourseScoreInteractor(View view, int postAssessmentScore) {
        CourseScorePresenter presenter = new CourseScorePresenter(view, postAssessmentScore);
    }

    public void saveData(DatabaseReference databaseReference, User user, int courseNo, int postAssessmentScore, int preAssessmentScore) {
        CoursesScores coursesScores = new CoursesScores();
        coursesScores.setCreatorId(user.getUserId());
        coursesScores.setCreatorName(user.getFirstName());
        coursesScores.setCourseNo(courseNo);
        coursesScores.setPostAssessmentScore(postAssessmentScore);
        coursesScores.setPreAssessmentScore(preAssessmentScore);
        coursesScores.setId(new IdGenerator().getCourseScoresId());
        databaseReference.child(CoursesScores.tableName).push().setValue(coursesScores);

        setCourseCounter(databaseReference, courseNo, postAssessmentScore, preAssessmentScore);
    }

    private void setCourseCounter(DatabaseReference databaseReference, int courseNo, int postAssessmentScore, int preAssessmentScore) {
        databaseReference.child(Statistics.table_name).runTransaction(new Transaction.Handler() {
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                Statistics statistics = mutableData.getValue(Statistics.class);
                statistics.updateScoreCount(courseNo, postAssessmentScore, preAssessmentScore);
                mutableData.setValue(statistics);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {
            }
        });
    }
}
