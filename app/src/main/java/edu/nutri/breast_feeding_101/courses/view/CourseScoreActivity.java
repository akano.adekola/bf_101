package edu.nutri.breast_feeding_101.courses.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

import com.google.firebase.database.DatabaseError;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.listener.FirebaseUserDataListener;
import edu.nutri.breast_feeding_101.common.util.CircleDisplay;
import edu.nutri.breast_feeding_101.common.view.BaseActivity;
import edu.nutri.breast_feeding_101.courses.interactor.CourseScoreInteractor;
import edu.nutri.breast_feeding_101.database.models.User;

/**
 * Created by ribads on 6/2/18.
 */
public class CourseScoreActivity extends BaseActivity implements CircleDisplay.SelectionListener, FirebaseUserDataListener {

    int postAssessmentScore, preAssessmentScore, courseNo;

    CourseScoreInteractor interactor;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        attachFirebaseUserListener(this);
        setContentView(R.layout.score_page, "Score");
        Bundle b = getIntent().getExtras();
        preAssessmentScore = b.getInt("preAssessmentScore");
        postAssessmentScore = b.getInt("postAssessmentScore");
        courseNo = b.getInt("courseNo");
        interactor = new CourseScoreInteractor(findViewById(android.R.id.content), postAssessmentScore);
    }

    public void home(View v){
        setResult(RESULT_OK);
        finish();
    }
    @Override
    public void onBackPressed(){
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onSelectionUpdate(float val, float maxval) {
        //		Toast.makeText(this, val +" 1 "+maxval, Toast.LENGTH_SHORT).show();
        Log.i("Main", "Selection update: " + val + ", max: " + maxval);
    }

    @Override
    public void onValueSelected(float val, float maxval) {
        //		Toast.makeText(this, val +" 2 "+maxval, Toast.LENGTH_SHORT).show();
        Log.i("Main", "Selection complete: " + val + ", max: " + maxval);
    }

    @Override
    public void onUserDataFetch(User user) {
        interactor.saveData(databaseReference, user, courseNo, postAssessmentScore, preAssessmentScore);
    }
}