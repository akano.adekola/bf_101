package edu.nutri.breast_feeding_101.courses.presenter;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.util.ValueUtil;

/**
 * Created by ribads on 6/2/18.
 */
public class CourseAssessmentPresenter {

    private Button option1Btn, option2Btn, option3Btn, option4Btn;
    private TextView score_text, time_text, timeUpTv, question;
    private RelativeLayout scoreLayout, livesLayout, quizHeaderlayout;
    private LinearLayout questionlayout;
    private Animation shake, blink;
    private ImageView liveImg1, liveImg2, liveImg3;
    private String[] questions, option_1, option_2, option_3, option_4, answer;

    public CourseAssessmentPresenter(View view, View.OnClickListener onClickListener) {
        questions = view.getResources().getStringArray(R.array.Questions);
        option_1 = view.getResources().getStringArray(R.array.option_1);
        option_2 = view.getResources().getStringArray(R.array.option_2);
        option_3 = view.getResources().getStringArray(R.array.option_3);
        option_4 = view.getResources().getStringArray(R.array.option_4);
        answer = view.getResources().getStringArray(R.array.answers);

        blink = AnimationUtils.loadAnimation(view.getContext(), R.anim.blink);
        shake = AnimationUtils.loadAnimation(view.getContext(), R.anim.fade_in);

        liveImg1 = view.findViewById(R.id.lives_img_1);
        liveImg2 = view.findViewById(R.id.lives_img_2);
        liveImg3 = view.findViewById(R.id.lives_img_3);
        timeUpTv = view.findViewById(R.id.time_up_tv);
        quizHeaderlayout = view.findViewById(R.id.quiz_header_layout);
        scoreLayout = view.findViewById(R.id.score_layout);
        livesLayout = view.findViewById(R.id.lives_layout);
        score_text = view.findViewById(R.id.main_title);
        time_text = view.findViewById(R.id.time_text);
        questionlayout = view.findViewById(R.id.question_layout);
        option1Btn = view.findViewById(R.id.option_1_btn);
        option2Btn = view.findViewById(R.id.option_2_btn);
        option3Btn = view.findViewById(R.id.option_3_btn);
        option4Btn = view.findViewById(R.id.option_4_btn);
        question = view.findViewById(R.id.textView2);

        time_text.setTextSize(20);
        timeUpTv.setVisibility(View.INVISIBLE);

        option1Btn.setOnClickListener(onClickListener);
        option2Btn.setOnClickListener(onClickListener);
        option3Btn.setOnClickListener(onClickListener);
        option4Btn.setOnClickListener(onClickListener);
    }

    public void hideQuizLayout(){
        scoreLayout.setVisibility(View.INVISIBLE);
        livesLayout.setVisibility(View.INVISIBLE);
    }

    public void startBlinkingText() {
        time_text.setTextSize(20);
        time_text.startAnimation(blink);
    }

    public void setTimeUptext() {
        questionlayout.startAnimation(shake);
        time_text.setTextSize(20);
    }

    public void setCdtText(long cdtText) {
        time_text.setText("00:" + ((cdtText / 1000) - 1));
    }

    public void clearAnimation() {
        questionlayout.clearAnimation();
        time_text.clearAnimation();
//        time_text.clearAnimation();
    }

    public void setQuestionOptions(int question_no, int random) {
        question.setText(question_no + ") " + questions[random]);
        option1Btn.setText(option_1[random]);
        option2Btn.setText(option_2[random]);
        option3Btn.setText(option_3[random]);
        option4Btn.setText(option_4[random]);
    }

    public String[] getOptions() {
        String[] options = new String[4];
        options[0] = ValueUtil.getText(option1Btn);
        options[0] = ValueUtil.getText(option1Btn);
        options[1] = ValueUtil.getText(option2Btn);
        options[2] = ValueUtil.getText(option3Btn);
        options[3] = ValueUtil.getText(option4Btn);
        return options;
    }

    public String getAnswer(int random) {
        return answer[random];
    }

    public void shakeQuestionLayout() {
        questionlayout.startAnimation(shake);
    }

    public void removeLivesImage() {
        if (liveImg1.getVisibility() == View.VISIBLE)
            liveImg1.setVisibility(View.GONE);
        else
            liveImg2.setVisibility(View.GONE);
    }

    public void setScoreText(int score) {
        ValueUtil.setText(score_text, score+"");
    }
}