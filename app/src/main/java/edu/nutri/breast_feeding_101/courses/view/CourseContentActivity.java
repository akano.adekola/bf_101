package edu.nutri.breast_feeding_101.courses.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.util.NotificationUtil;
import edu.nutri.breast_feeding_101.common.view.BaseActivity;

public class CourseContentActivity extends BaseActivity implements View.OnClickListener{
	TextView courseTv, subTitleTv, prevTv, nextTv;
	String courseTitle;
	int preAssessmentScore;
	int courseNo;
	int currentPage;
    String[] courseContents;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        preAssessmentScore = bundle.getInt("preAssessmentScore");
        courseNo = bundle.getInt("courseNo");
        courseTitle = getResources().getStringArray(R.array.course_titles)[courseNo-1];
        setContentView(R.layout.activity_course_content, courseTitle);
        courseContents = getCourseContent();
		initViews();
	}

    private void initViews() {
        subTitleTv = findViewById(R.id.title_tv);
        courseTv = findViewById(R.id.course_tv);
        prevTv = findViewById(R.id.prev_tv);
        nextTv = findViewById(R.id.next_tv);
        courseTv.setText(courseContents[0]);
        prevTv.setOnClickListener(this);
        nextTv.setOnClickListener(this);
        currentPage = 0;
        setCourseSubtitles();
	}

    @Override
	public void onBackPressed(){
        showExitConfirmation();
	}

    @Override
    public void onClick(View v) {
	    int id = v.getId();
	    int nextId = R.id.next_tv;

        if (currentPage == 0 && id == R.id.prev_tv){
            showExitConfirmation();
        }
        else if (currentPage == courseContents.length-1 && id == R.id.next_tv){
            Bundle bundle = new Bundle();
            bundle.putInt("preAssessmentScore", preAssessmentScore);
            bundle.putInt("courseNo", courseNo);

            Intent intent = new Intent(this, CourseAssessmentActivity.class);
            intent.putExtras(bundle);

            startActivityForResult(intent, 975);
        }
        else {
            if (id == nextId)
                currentPage++;
            else
                currentPage--;

            if (currentPage == 0)
                prevTv.setText("Exit");
            else
                prevTv.setText("Previous");

            if (currentPage == courseContents.length-1 )
                nextTv.setText("Finish");
            else
                nextTv.setText("Next");

            courseTv.setText(courseContents[currentPage]);
                setCourseSubtitles();
        }

    }

    private void setCourseSubtitles() {
	    String subTitle = "";
        String[] courseSubTitle;
	    if (courseNo == 5){
            courseSubTitle = getResources().getStringArray(R.array.course_5_sub_titles);
            if (currentPage == 0 || currentPage == 1 ){
                subTitle = courseSubTitle[0];
            }
            else if (currentPage == 2 || currentPage == 3 ){
                subTitle = courseSubTitle[1];
            }
            else {
                subTitle = courseSubTitle[2];
            }
        }
        else if (courseNo == 6){
            courseSubTitle = getResources().getStringArray(R.array.course_6_sub_titles);
            if (currentPage == 0 || currentPage == 1 || currentPage == 2 ){
                subTitle = courseSubTitle[0];
            }
            else {
                subTitle = courseSubTitle[1];
            }
        }
        subTitleTv.setText(subTitle);
    }

    private String[] getCourseContent() {
        String[] courseContents = null;
            switch (courseNo) {
                case 1:
                    courseContents = getResources().getStringArray(R.array.course_1_notes);
                    break;
                case 2:
                    courseContents = getResources().getStringArray(R.array.course_2_notes);
                    break;
                case 3:
                    courseContents = getResources().getStringArray(R.array.course_3_notes);
                    break;
                case 4:
                    courseContents = getResources().getStringArray(R.array.course_4_notes);
                    break;
                case 5:
                    courseContents = getResources().getStringArray(R.array.course_5_notes);
                    break;
                case 6:
                    courseContents = getResources().getStringArray(R.array.course_6_notes);
                    break;
            }
            return courseContents;
    }

    public void showExitConfirmation(){
        NotificationUtil.showConfirmationDialog(getSupportFragmentManager(), "Are you sure You want to exit?", v -> {
            setResult(RESULT_CANCELED);
            finish();
        }, v -> {
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED){
            setResult(RESULT_CANCELED);
            finish();
        }
        else {
            setResult(RESULT_OK);
            finish();
        }
    }
}