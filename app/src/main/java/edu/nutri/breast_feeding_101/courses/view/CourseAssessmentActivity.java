package edu.nutri.breast_feeding_101.courses.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

import java.util.ArrayList;
import java.util.List;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.util.IdGenerator;
import edu.nutri.breast_feeding_101.common.util.NotificationUtil;
import edu.nutri.breast_feeding_101.common.view.BaseActivity;
import edu.nutri.breast_feeding_101.courses.interactor.CourseAssessmentInteractor;
import edu.nutri.breast_feeding_101.database.models.QuizLeaderBoard;
import edu.nutri.breast_feeding_101.database.models.Statistics;
import edu.nutri.breast_feeding_101.database.models.User;

public class CourseAssessmentActivity extends BaseActivity implements View.OnClickListener{

    int question_no, selectedRandomQuestion, score, lives, courseNo, preAssessmentScore;
    List<Integer> answeredQuestions;
    boolean isPreAssessment, isQuiz;
    CourseAssessmentInteractor interactor;
    String title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        isPreAssessment = bundle.getBoolean("isPreAssessment");
        if (!isPreAssessment) {
            preAssessmentScore = bundle.getInt("preAssessmentScore");
            title = "Post Assessment";
        }
        else
            title = "Pre Assessment";
        courseNo = bundle.getInt("courseNo");
        if (courseNo == 0) {
            title = "Quiz";
            isQuiz = true;
        }
        else
            isQuiz = false;
        setContentView(R.layout.activity_course_assessment, title);
        interactor = new CourseAssessmentInteractor(findViewById(android.R.id.content), this);
        if (!isQuiz)
            interactor.hideQuizLayout();
        answeredQuestions = new ArrayList<Integer>();
        question_no = 0;
        score = 0;
        lives = 3;
        interactor.initTimer();
        continueAssessment();
    }

    private void pickRandomQuestion() {
//        // TODO: 6/2/18 Remove
//        if (answeredQuestions.size() == 57)
//            answeredQuestions.clear();
        selectedRandomQuestion = interactor.getRandomNumber(courseNo);
        if (answeredQuestions.contains(selectedRandomQuestion))
            pickRandomQuestion();
        else
            continueAssessment();
    }

    private void continueAssessment() {
        interactor.startTimer();
        interactor.clearAnimations();
        question_no++;
        interactor.setQuestionOptions(question_no, selectedRandomQuestion);
        answeredQuestions.add(selectedRandomQuestion);
    }

    private void exitAssessment() {
        Intent intent;
        if (isQuiz){
            saveQuizScore();
            intent = new Intent(this, QuizLeaderBoardActivity.class);
            intent.putExtra("quizScore", score);
        }
        else {
            if (isPreAssessment) {
                intent = new Intent(this, CourseContentActivity.class);
                intent.putExtra("preAssessmentScore", score);
            } else {
                intent = new Intent(this, CourseScoreActivity.class);
                intent.putExtra("postAssessmentScore", score);
                intent.putExtra("preAssessmentScore", preAssessmentScore);
            }
            intent.putExtra("courseNo", courseNo);
        }
        startActivityForResult(intent, 847);
        interactor.cancelTimer();
        interactor.clearAnimations();
    }

    private void saveQuizScore() {
        QuizLeaderBoard quizLeaderBoard = new QuizLeaderBoard();
        quizLeaderBoard.setCreatorName(user.getFirstName());
        quizLeaderBoard.setScore(-score);
        quizLeaderBoard.setCreatorId(userId);
        quizLeaderBoard.setId(new IdGenerator().getQuizLeaderBoardId());

        databaseReference.child(QuizLeaderBoard.tableName).push().setValue(quizLeaderBoard);

        updateQuizCount(score);
    }

    private void updateQuizCount(int score) {
        databaseReference.child(Statistics.table_name).runTransaction(new Transaction.Handler() {
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                Statistics statistics = mutableData.getValue(Statistics.class);
                statistics.updateQuizCount(score);
                mutableData.setValue(statistics);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {
            }
        });
    }

    @Override
    public void onClick(View view) {
        interactor.cancelTimer();
        interactor.setButtonClickedBg(view);
        String selected_answer = interactor.getSelectedOption(view.getId());
        boolean isAnswerCorrect = selected_answer.equals(interactor.getAnswer(selectedRandomQuestion));
        if (isAnswerCorrect)
            score+=20;
        if (isQuiz && !isAnswerCorrect) {
            lives--;
            interactor.removeLivesImage();
        }
        interactor.shakeQuestionLayout();

        if (isQuiz) {
            interactor.setScoreText(score);
            if (lives == 0)
                exitAssessment();
            else
                pickRandomQuestion();
        }
        else {
            if (question_no == 5)
                exitAssessment();
            else
                pickRandomQuestion();
        }
    }

    @Override
    public void onBackPressed() {
        showExitConfirmation();
    }

    public void showExitConfirmation(){
        NotificationUtil.showConfirmationDialog(getSupportFragmentManager(), "Are you sure You want to exit?", v -> {
            setResult(RESULT_CANCELED);
            finish();
            interactor.cancelTimer();
        }, v -> {

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED){
            setResult(RESULT_CANCELED);
            finish();
        }
        else {
            setResult(RESULT_OK);
            finish();
        }
    }
}