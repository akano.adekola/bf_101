package edu.nutri.breast_feeding_101.courses.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.util.Constants;
import edu.nutri.breast_feeding_101.common.util.ValueUtil;
import edu.nutri.breast_feeding_101.common.view.BaseActivity;
import edu.nutri.breast_feeding_101.courses.adapter.LeaderBoardAdapter;
import edu.nutri.breast_feeding_101.dashboard.GlideApp;
import edu.nutri.breast_feeding_101.database.models.QuizLeaderBoard;

public class QuizLeaderBoardActivity extends BaseActivity {

    RecyclerView leaderBoardRecycler;
    LeaderBoardAdapter leaderBoardAdapter;
    TextView  firstPositionNameTv, firstPositionScoreTv, secondPositionNameTv, secondPositionScoreTv, thirdPositionNameTv, thirdPositionScoreTv;
    ImageView firstImg, secondImg, thirdImg;
    List<String> nameList;
    List<String> userIdList;
    List<Integer> scoreList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_leader_board, "Leader Board");

        nameList = new ArrayList<>();
        scoreList = new ArrayList<>();
        userIdList = new ArrayList<>();
        initViews();
    }

    private void initViews() {
        firstPositionNameTv = findViewById(R.id.first_position_name_tv);
        firstPositionScoreTv = findViewById(R.id.first_position_score_tv);
        firstImg = findViewById(R.id.first_position_img);
        secondPositionNameTv = findViewById(R.id.second_position_name_tv);
        secondPositionScoreTv = findViewById(R.id.second_position_score_tv);
        secondImg = findViewById(R.id.second_position_img);
        thirdPositionNameTv = findViewById(R.id.third_position_name_tv);
        thirdPositionScoreTv = findViewById(R.id.third_position_score_tv);
        thirdImg = findViewById(R.id.third_position_img);
        leaderBoardRecycler = findViewById(R.id.leader_board_recycler);
        leaderBoardAdapter = new LeaderBoardAdapter();
        leaderBoardRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        leaderBoardRecycler.setAdapter(leaderBoardAdapter);
        getLeaderBoard();
//        getHighestScore();

//        ValueUtil.setText(quizScoreTv, quizScore+"");
    }

//    private void getHighestScore() {
//        databaseReference.child(QuizLeaderBoard.tableName).orderByChild("userId").equalTo(userId).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                getHighestScoreFromList(dataSnapshot);
//            }
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//    }
//
//    private void getHighestScoreFromList(DataSnapshot dataSnapshot) {
//        int highestScore = 0;
//        for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
//            int score = dataSnapshot2.getValue(QuizLeaderBoard.class).getScore() * -1;
//            if (score > highestScore)
//                highestScore = score;
//        }
////        highestScoreTv.setText(highestScore+"");
//    }

    private void getLeaderBoard() {

        databaseReference.child(QuizLeaderBoard.tableName).orderByChild("score").limitToFirst(10).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()){
                    QuizLeaderBoard quizLeaderBoard = dataSnapshot2.getValue(QuizLeaderBoard.class);
                    userIdList.add(quizLeaderBoard.getCreatorId());
                    nameList.add(quizLeaderBoard.getCreatorName());
                    scoreList.add(quizLeaderBoard.getScore() * -1);
                    if (nameList.size() == 10)
                        setTopScorers();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void setTopScorers() {
        firstPositionNameTv.setText(userIdList.get(0).equals(userId) ? "You" : nameList.get(0));
        setImage(userIdList.get(0), firstImg);
        secondPositionNameTv.setText(userIdList.get(1).equals(userId) ? "You" : nameList.get(1));
        setImage(userIdList.get(1), secondImg);
        thirdPositionNameTv.setText(userIdList.get(2).equals(userId) ? "You" : nameList.get(2));
        setImage(userIdList.get(2), thirdImg);

        firstPositionScoreTv.setText(scoreList.get(0)+"");
        secondPositionScoreTv.setText(scoreList.get(1)+"");
        thirdPositionScoreTv.setText(scoreList.get(2)+"");

        removeTopScorers();

        leaderBoardAdapter.updateList(nameList, scoreList, userIdList);
    }

    private void removeTopScorers() {
        for (int x = 1; x < 4; x++){
            userIdList.remove(0);
            nameList.remove(0);
            scoreList.remove(0);
        }
    }

    private void setImage(String scoreUserId, ImageView profileImg) {
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(Constants.PROFILE_IMAGE).child(scoreUserId);

        GlideApp.with(this)
                .load(storageReference)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.help_white_108x108)
                .placeholder(R.drawable.help_white_108x108)
                .into(profileImg);
    }
}