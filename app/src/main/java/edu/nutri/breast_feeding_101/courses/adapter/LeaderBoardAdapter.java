package edu.nutri.breast_feeding_101.courses.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.courses.adapter.holder.LeaderBoardHolder;

/**
 * Created by ribads on 6/3/18.
 */
public class LeaderBoardAdapter extends RecyclerView.Adapter<LeaderBoardHolder> {
    private List<String> nameList = new ArrayList<>();
    private List<Integer> scoreList = new ArrayList<>();
    private List<String> userIdList;

    @NonNull
    @Override
    public LeaderBoardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_leader_board, parent, false);
            return new LeaderBoardHolder(view);
    }

    @Override
    public void onBindViewHolder(LeaderBoardHolder holder, int position) {
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String scoreUserId = userIdList.get(position);
        String name = scoreUserId.equals(userId) ? "You" : nameList.get(position);
        String score = scoreList.get(position)+"";
        holder.bindData(name, score, position, scoreUserId);
    }

    @Override
    public int getItemCount() {
        return nameList.size();
    }

    public void updateList(List<String> nameList, List<Integer> scoreList, List<String> userIdList) {
        this.userIdList = userIdList;
        this.nameList = nameList;
        this.scoreList = scoreList;
        notifyDataSetChanged();
    }
}