package edu.nutri.breast_feeding_101.courses.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.util.Constants;
import edu.nutri.breast_feeding_101.dashboard.GlideApp;
import edu.nutri.breast_feeding_101.database.models.QuizLeaderBoard;

/**
 * Created by ribads on 6/3/18.
 */
public class LeaderBoardHolder extends RecyclerView.ViewHolder {

    private TextView nameTv, scoreTv, numberTv;
    private ImageView profileImg;

    public LeaderBoardHolder(View itemView) {
        super(itemView);
        nameTv = itemView.findViewById(R.id.name_tv);
        scoreTv = itemView.findViewById(R.id.score_tv);
        numberTv = itemView.findViewById(R.id.number_tv);
        profileImg = itemView.findViewById(R.id.profile_img);
    }

    public void bindData(String name, String score, int position, String scoreUserId) {
        int number = position + 4;
        numberTv.setText(number+"");
        nameTv.setText(name);
        scoreTv.setText(score+"");
        showImage(scoreUserId);
    }

    private void showImage(String scoreUserId) {
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(Constants.PROFILE_IMAGE).child(scoreUserId);

        GlideApp.with(itemView.getContext())
                .load(storageReference)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.help_white_108x108)
                .placeholder(R.drawable.help_white_108x108)
                .into(profileImg);
    }
}