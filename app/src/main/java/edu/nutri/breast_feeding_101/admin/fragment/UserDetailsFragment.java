package edu.nutri.breast_feeding_101.admin.fragment;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.admin.adapter.UserDetailsAdapter;
import edu.nutri.breast_feeding_101.common.util.NotificationUtil;
import edu.nutri.breast_feeding_101.common.util.ValueUtil;
import edu.nutri.breast_feeding_101.database.models.User;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserDetailsFragment extends DialogFragment {

    User user;
    boolean admin;
    UserDetailsAdapter adapter;
    RecyclerView recyclerView;
    TextView adminTv;
    ImageView adminImg;
    public UserDetailsFragment() {
        // Required empty public constructor
    }

    public void addData(User user) {
        this.user = user;
        this.admin = user.isAdmin();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_details, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        (view.findViewById(R.id.delete_layout)).setOnClickListener(v -> {
            deleteUser();
        });
        (view.findViewById(R.id.admin_layout)).setOnClickListener(v -> {
            performAdminFunction();
        });
        adminImg = view.findViewById(R.id.admin_img);
        adminTv = view.findViewById(R.id.admin_tv);

        recyclerView = view.findViewById(R.id.user_details_recycler);
        adapter = new UserDetailsAdapter();

        String firstName = user.getFirstName() != null ? user.getFirstName() : "";
        String lastName = user.getLastName() != null ? user.getLastName() : "";
        ((TextView)view.findViewById(R.id.name_tv)).setText(firstName +" "+ lastName);

        if (admin){
            adminTv.setText("Remove Admin");
            adminImg.setImageDrawable(view.getResources().getDrawable(R.drawable.account_remove));
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        setAdapterData();
    }

    private void deleteUser() {
        NotificationUtil.showConfirmationDialog(getFragmentManager(), "Are you sure you want to delete this user", v -> {
            FirebaseDatabase.getInstance().getReference().child("Test").child(User.tableName).child(user.getUserId()).removeValue();
            dismiss();
        }, v -> {
        });
    }

    private void performAdminFunction() {
        if (admin)
            user.setAdmin(false);
        else
            user.setAdmin(true);
        FirebaseDatabase.getInstance().getReference().child("Test").child(User.tableName).child(user.getUserId()).setValue(user);
        dismiss();
    }

    private void setAdapterData() {
        List<String> titleList = new ArrayList<>();
        titleList.add("Email: ");
        titleList.add("Gender: ");
        titleList.add("No of Children: ");
        titleList.add("Date Of Birth: ");
        titleList.add("Level of Education: ");
        titleList.add("Marital Status: ");
        titleList.add("Age: ");
        titleList.add("Joined At: ");
        titleList.add("Id: ");

        List<String> dataList = new ArrayList<>();
        dataList.add(user.getEmail());
        dataList.add(user.getGender());
        dataList.add(user.getNoOfChildren());
        dataList.add(user.getDateOfBirth("YYYY - MM - dd"));
        dataList.add(user.getLevelOfEducation());
        dataList.add(user.getMaritalStatus());
        String age = ValueUtil.getAge(user.getDateOfBirth()) + "";
        dataList.add(age.equals("0") ? "-" : age);
        dataList.add(user.getCreatedDate("dd/MM/YYYY HH:mm"));
        dataList.add(user.getUserId()+"");

        adapter.setData(titleList, dataList);
    }

}