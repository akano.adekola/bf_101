package edu.nutri.breast_feeding_101.admin.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.admin.adapter.holder.UserDetailsHolder;
import edu.nutri.breast_feeding_101.database.models.User;

/**
 * Created by ribads on 7/17/18.
 */
public class UserDetailsAdapter extends RecyclerView.Adapter<UserDetailsHolder>{
    private List<String> titleList, dataList;
    @NonNull
    @Override
    public UserDetailsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_user_details, parent, false);
        return new UserDetailsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserDetailsHolder holder, int position) {
        String title = titleList.get(position);
        String data = dataList.get(position);
        holder.bindData(title, data);
    }

    @Override
    public int getItemCount() {
        return titleList == null ? 0 : titleList.size();
    }

    public void setData(List<String> titleList, List<String> dataList) {
        this.titleList = titleList;
        this.dataList = dataList;
    }
}