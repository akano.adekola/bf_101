package edu.nutri.breast_feeding_101.admin.presenter;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import edu.nutri.breast_feeding_101.R;

/**
 * Created by ribads on 7/17/18.
 */
public class AdminPresenter {

    //    private TextView
    public AdminPresenter(View view, View.OnClickListener listener) {
        LinearLayout userLayout = view.findViewById(R.id.user_layout);
        userLayout.setOnClickListener(listener);
    }

}