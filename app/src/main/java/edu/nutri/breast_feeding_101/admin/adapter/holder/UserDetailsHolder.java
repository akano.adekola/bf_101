package edu.nutri.breast_feeding_101.admin.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.database.models.User;

/**
 * Created by ribads on 7/17/18.
 */
public class UserDetailsHolder extends RecyclerView.ViewHolder {

    private TextView titleTv, dataTv;
    public UserDetailsHolder(View itemView) {
        super(itemView);
        titleTv = itemView.findViewById(R.id.title_tv);
        dataTv = itemView.findViewById(R.id.data_tv);
    }

    public void bindData(String title, String data) {
        titleTv.setText(title);
        dataTv.setText(data.equals("") ? "-" : data);
    }
}