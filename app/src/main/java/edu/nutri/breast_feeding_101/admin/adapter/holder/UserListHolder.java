package edu.nutri.breast_feeding_101.admin.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.enums.Enums;
import edu.nutri.breast_feeding_101.common.enums.Gender;
import edu.nutri.breast_feeding_101.common.listener.OnItemClickListener;
import edu.nutri.breast_feeding_101.database.models.User;

/**
 * Created by ribads on 7/17/18.
 */
public class UserListHolder extends RecyclerView.ViewHolder {

    private ImageView genderImg;
    private TextView nameTv, emailTv;

    public UserListHolder(View itemView) {
        super(itemView);
        genderImg = itemView.findViewById(R.id.gender_img);
        nameTv = itemView.findViewById(R.id.name_tv);
        emailTv = itemView.findViewById(R.id.email_tv);
    }

    public void bindData(User user, OnItemClickListener<Integer> listener, int position) {
        String firstName = user.getFirstName() != null ? user.getFirstName() : "";
        String lastName = user.getLastName() != null ? user.getLastName() : "";
        String email = user.getEmail();
        String gender = user.getGender();

        if (gender != null && !gender.equals("")) {
            Gender genderValue = Gender.valueOf(gender);
            int genderImage = genderValue == Gender.Male ? R.drawable.gender_male : R.drawable.gender_female;
            genderImg.setImageResource(genderImage);
        }

        nameTv.setText(firstName +" "+ lastName);
        emailTv.setText(email);
        itemView.setOnClickListener(v -> listener.onItemClick(position));
    }
}