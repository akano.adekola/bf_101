package edu.nutri.breast_feeding_101.admin.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.admin.adapter.UserListAdapter;
import edu.nutri.breast_feeding_101.admin.fragment.UserDetailsFragment;
import edu.nutri.breast_feeding_101.common.listener.OnItemClickListener;
import edu.nutri.breast_feeding_101.common.util.NotificationUtil;
import edu.nutri.breast_feeding_101.common.view.BaseActivity;
import edu.nutri.breast_feeding_101.database.models.User;

public class UserListActivity extends BaseActivity implements OnItemClickListener<Integer> {

    RecyclerView recyclerView;
    UserListAdapter adapter;
    ArrayList<User> userArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list, "User List");
        initViews();
        initData();
    }

    private void initViews() {
        adapter = new UserListAdapter();
        adapter.attachListener(this);
        recyclerView = findViewById(R.id.user_list_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    private void initData() {
        usersDatabaseReference.orderByChild("email").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userArrayList = new ArrayList<>();
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    userArrayList.add(snapshot.getValue(User.class));
                }
                NotificationUtil.showToast(getApplicationContext(), userArrayList.size()+" Users");
                adapter.addData(userArrayList);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

    }

    @Override
    public void onItemClick(Integer position) {
        User user = userArrayList.get(position);
        UserDetailsFragment userDetailsFragment = new UserDetailsFragment();
        userDetailsFragment.addData(user);
        userDetailsFragment.show(getSupportFragmentManager(), "ss");
    }
}