package edu.nutri.breast_feeding_101.admin.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.admin.interactor.AdminInteractor;
import edu.nutri.breast_feeding_101.admin.view.UserListActivity;
import edu.nutri.breast_feeding_101.common.listener.FirebaseUserDataListener;
import edu.nutri.breast_feeding_101.common.util.LauncherUtil;
import edu.nutri.breast_feeding_101.common.view.BaseFragment;
import edu.nutri.breast_feeding_101.database.models.User;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdminFragment extends BaseFragment implements FirebaseUserDataListener, View.OnClickListener {

    AdminInteractor interactor;

    public AdminFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return getFragmentView(inflater, R.layout.fragment_admin, container, this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        interactor =  new AdminInteractor(view, this);
    }

    @Override
    public void onUserDataFetch(User user) {

    }

    @Override
    public void onClick(View v) {
        LauncherUtil.launchClass(getActivity(), UserListActivity.class);
    }
}