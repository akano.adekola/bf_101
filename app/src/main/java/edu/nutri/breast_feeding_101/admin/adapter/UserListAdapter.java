package edu.nutri.breast_feeding_101.admin.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.admin.adapter.holder.UserListHolder;
import edu.nutri.breast_feeding_101.common.listener.OnItemClickListener;
import edu.nutri.breast_feeding_101.database.models.User;

/**
 * Created by ribads on 7/17/18.
 */
public class UserListAdapter extends RecyclerView.Adapter<UserListHolder> {

    private ArrayList<User> userArrayList;
    private OnItemClickListener<Integer> listener;

    @NonNull
    @Override
    public UserListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_user_list, parent, false);
        return new UserListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserListHolder holder, int position) {
        User user = userArrayList.get(position);
        holder.bindData(user, listener, position);
    }

    @Override
    public int getItemCount() {
        return userArrayList == null ? 0 : userArrayList.size();
    }

    public void addData(ArrayList<User> userArrayList) {
        this.userArrayList = userArrayList;
        notifyDataSetChanged();
    }

    public void attachListener(OnItemClickListener<Integer> listener) {
        this.listener = listener;
    }
}
