package edu.nutri.breast_feeding_101.admin.interactor;

import android.view.View;

import edu.nutri.breast_feeding_101.admin.fragment.AdminFragment;
import edu.nutri.breast_feeding_101.admin.presenter.AdminPresenter;

/**
 * Created by ribads on 7/17/18.
 */
public class AdminInteractor {

    AdminPresenter presenter;

    public AdminInteractor(View view, View.OnClickListener listener) {
        presenter = new AdminPresenter(view, listener);
    }
}
