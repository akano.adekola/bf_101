package edu.nutri.breast_feeding_101.common.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import edu.nutri.breast_feeding_101.R;

/**
 * Created by Akano on 1/24/2018.
 */

public class PickerItemHolder extends RecyclerView.ViewHolder {

    public TextView mItemView;

    public PickerItemHolder(View view){
        super(view);
        mItemView = (TextView) view.findViewById(R.id.list_item);
    }
}