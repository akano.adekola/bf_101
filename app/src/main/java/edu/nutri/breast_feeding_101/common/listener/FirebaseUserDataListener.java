package edu.nutri.breast_feeding_101.common.listener;

import edu.nutri.breast_feeding_101.database.models.User;

/**
 * Created by ribads on 7/14/18.
 */
public interface FirebaseUserDataListener {
    void onUserDataFetch(User user);
}
