package edu.nutri.breast_feeding_101.common.enums;

/**
 * Created by ribads on 7/21/18.
 */
public enum  MaritalStatus {

    Single, Married, Not_specified;

    public static String[] getMaritalStatus(){
        String[] maritalStatusStrings = new String[MaritalStatus.values().length];
        int x = 0;
        for (MaritalStatus status : MaritalStatus.values()){
            maritalStatusStrings[x] = getMaritalStatusValue(status);
            x++;
        }
        return maritalStatusStrings;
    }

    private static String getMaritalStatusValue(MaritalStatus status) {
        switch (status){
            case Single:
                return "Single";
            case Married:
                return "Married";
            case Not_specified:
                return "Not specified";
            default:
                return "Error";
        }
    }

}
