package edu.nutri.breast_feeding_101.common.util;

import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ribads on 4/2/18.
 */

public class ValueUtil {

    public static String getText(TextView textView){
        return textView.getText().toString().trim();
    }

    public static void setText(TextView mTextView, String value){
        if (mTextView != null && value != null){
            mTextView.setText(value);
        }
    }

    public static Calendar getCalendar(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    public static void clearText(TextView textView) {
        textView.setText("");
    }

    public static String getDateFromEpoch(Long epochDate, String pattern){
        Date date = new Date(epochDate);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    public static String getDateFromEpoch(Long epochDate){
        Date date = new Date(epochDate);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm, dd/MM/yy");
        return simpleDateFormat.format(date);
    }

    public static boolean isEmpty(String s) {
        return s.equals("");
    }

    public static int getAge(Long dateOfBirth) {
        if (dateOfBirth == null)
            return 0;
        Date date = new Date(dateOfBirth);
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        calendar.setTime(date);
        int yearOfBirth = calendar.get(Calendar.YEAR);
        return currentYear - yearOfBirth;
    }

    public static Long getEpoch(Calendar calendar) {
        return calendar.getTime().getTime();
    }
}