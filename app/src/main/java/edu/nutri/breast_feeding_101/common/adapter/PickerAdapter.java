package edu.nutri.breast_feeding_101.common.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.adapter.holder.PickerItemHolder;
import edu.nutri.breast_feeding_101.common.listener.OnItemClickListener;

/**
 * Created by Akano on 1/24/2018.
 */

public class PickerAdapter extends RecyclerView.Adapter<PickerItemHolder> {

    private ArrayList<String> items;
    private OnItemClickListener<Integer> listener;
    private List<Integer> selectedIndies;
    private boolean multiple = false;

    public PickerAdapter(ArrayList<String> items, OnItemClickListener<Integer> listener) {
        this.selectedIndies = new ArrayList<>();
        this.items = items;
        this.listener = listener;
    }

    public PickerAdapter(ArrayList<String> items, List<String> selectedItems, OnItemClickListener<Integer> listener) {
        this.items = items;
        this.listener = listener;
        this.selectedIndies = new ArrayList<>();
        updateIndies(selectedItems);
    }

    @Override
    public PickerItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_picker_item, parent, false);
        return new PickerItemHolder(view);
    }

    private void updateIndies(List<String> selectedItems) {
        if (selectedItems != null && selectedItems.size() > 0) {
            for (int i = 0; i < selectedItems.size(); i++) {
                int index = items.indexOf(selectedItems.get(i));
                if (index > 0)
                    selectedIndies.add(index);
            }
        }
    }

    public List<Integer> getSelectedIndies() {
        return selectedIndies;
    }

    public void setSelectedIndies(List<Integer> selectedIndies) {
        this.selectedIndies = selectedIndies;
    }

    public void setMultiple(boolean value) {
        this.multiple = value;
    }

    @Override
    public void onBindViewHolder(final PickerItemHolder holder, final int position) {
        holder.mItemView.setText(items.get(position));
        holder.mItemView.setTextColor(multiple ? holder.itemView.getContext().getResources().getColor(android.R.color.darker_gray)
                : holder.itemView.getContext().getResources().getColor(android.R.color.black));
        if (multiple && selectedIndies != null && selectedIndies.size() > 0) {
            int index = selectedIndies.indexOf(position);
            holder.mItemView.setTextColor(index > -1 ? holder.itemView.getContext().getResources().getColor(android.R.color.black) :
                    holder.itemView.getContext().getResources().getColor(android.R.color.darker_gray));
        }

        holder.itemView.setOnClickListener(v -> {
            if (multiple) {
                int selectedIndex = selectedIndies.indexOf(position);
                boolean selected = selectedIndex > -1;
                holder.mItemView.setTextColor(selected
                        ? v.getContext().getResources().getColor(android.R.color.black)
                        : v.getContext().getResources().getColor(android.R.color.black));
                if (selected) {
                    selectedIndies.remove(selectedIndex);
                } else {
                    selectedIndies.add(position);
                }
            } else if (listener != null)
                listener.onItemClick(position);
        });
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }
}