package edu.nutri.breast_feeding_101.common.enums;

/**
 * Created by ribads on 7/21/18.
 */
public enum LevelOfEducation {

    No_formal_education, primary_education, secondary_education, tertiary_education, Not_specified;

    public static String[] getLevelOfEducation() {
        String[] levelOfEducation = new String[LevelOfEducation.values().length];
        int x = 0;
        for (LevelOfEducation education : LevelOfEducation.values()){
            levelOfEducation[x] = getLevelOfEducationValues(education);
            x++;
        }
        return levelOfEducation;
    }

    private static String getLevelOfEducationValues(LevelOfEducation education) {
        switch (education){
            case No_formal_education:
                return "No formal education";
            case primary_education:
                return "primary education";
            case secondary_education:
                return "secondary education";
            case tertiary_education:
                return "tertiary education";
            case Not_specified:
                return "Not specified";
            default:
                return "Error";
        }
    }

}
