package edu.nutri.breast_feeding_101.common.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.firebase.client.Firebase;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import edu.nutri.breast_feeding_101.BuildConfig;
import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.auth.view.AuthActivity;
import edu.nutri.breast_feeding_101.common.listener.FirebaseUserDataListener;
import edu.nutri.breast_feeding_101.common.util.LauncherUtil;
import edu.nutri.breast_feeding_101.common.util.NotificationUtil;
import edu.nutri.breast_feeding_101.database.models.User;

/**
 * Created by ribads on 4/2/18.
 */

public class BaseActivity extends AppCompatActivity {

    boolean connectivity = false;
    public FirebaseDatabase database;
    public FirebaseUser firebaseUser;
    public FirebaseAuth firebaseAuth;
    public DatabaseReference databaseReference;
    public DatabaseReference usersDatabaseReference;
    public String email, userId, firstName;
    public User user;
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    public StorageReference storageReference = storage.getReference();
    public FirebaseUserDataListener firebaseUserDataListener;

    @Override
    public void setContentView(int layoutResID) {
        setContentView(layoutResID, "");
    }

    public void attachFirebaseUserListener(FirebaseUserDataListener firebaseUserDataListener){
        this.firebaseUserDataListener = firebaseUserDataListener;
    }

    public void setContentView(int layoutResID, String titleBarText) {
        setContentView(layoutResID, titleBarText, true);
    }

    public void setContentView(int layoutResID, String titleBarText, boolean back) {
        firebaseInstances();
        super.setContentView(layoutResID);
        initBar(titleBarText, back);
    }

    private void initBar(String titleBarText, boolean back) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (!isTaskRoot() && back)
                actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(titleBarText);
            actionBar.setElevation(1);
        }
    }

    public void firebaseInstances() {
        database = FirebaseDatabase.getInstance();
        Firebase.setAndroidContext(this);
        usersDatabaseReference = database.getReference().child("Test").child(User.tableName);
        if (BuildConfig.DEBUG)
            databaseReference = database.getReference().child("Test");
        else
            databaseReference = database.getReference().child("Live");
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        if (firebaseUser!=null) {
            email = firebaseUser.getEmail();
            userId = firebaseUser.getUid();
        }
        if (firebaseUser != null)
            getCurrentUserDetails();
    }

    public void getCurrentUserDetails() {
        usersDatabaseReference.child(userId).addListenerForSingleValueEvent(new com.google.firebase.database.ValueEventListener() {
            @Override
            public void onDataChange(@NonNull com.google.firebase.database.DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    user = dataSnapshot.getValue(User.class);
                    if (firebaseUserDataListener != null)
                        firebaseUserDataListener.onUserDataFetch(user);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void logOut(Context context){
        //firebase
        firebaseAuth.signOut();
        //facebook
        if (LoginManager.getInstance() != null)
            LoginManager.getInstance().logOut();
        //google
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(context, gso);
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(context);
        if (account != null)
            mGoogleSignInClient.signOut();

        LauncherUtil.launchClass(context, AuthActivity.class);
    }

    public void showProgress(String message){
        showProgress(message, false);
    }

    public void showProgress(String message, Boolean cancelable){
        final ViewGroup viewGroup = (ViewGroup) getWindow().getDecorView();
        final View progressView = LayoutInflater.from(this).inflate(R.layout.progress_view, null, true);
        progressView.setTag(780);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        viewGroup.addView(progressView, params);
        progressView.setVisibility(View.VISIBLE);
        TextView textView = progressView.findViewById(R.id.message);
        if (message != null && textView != null) {
            textView.setVisibility(View.VISIBLE);
            textView.setText(message);
        }
    }

    public void hideProgress(){
        final ViewGroup viewGroup = (ViewGroup) getWindow().getDecorView();
        View progressView = viewGroup.findViewWithTag(780);
        if (progressView != null) {
            progressView.setVisibility(View.GONE);
            viewGroup.removeView(progressView);
        }
    }

//    public void Check_firebase() {
//        Firebase connectedRef = new Firebase(UserDetails.database_url + ".info/connected");
//        connectedRef.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot snapshot) {
//                boolean connected = snapshot.getValue(Boolean.class);
//                if (connected)
//                    connectivity = true;
//                else
//                    connectivity = false;
//            }
//            @Override
//            public void onCancelled(FirebaseError error) {
////                System.err.println("Listener was cancelled");
//            }
//        });
//    }
}
