package edu.nutri.breast_feeding_101.common.listener;

/**
 * Created by Akano on 1/24/2018.
 */

public interface OnItemClickListener<T> {
     void onItemClick(T t);
}
