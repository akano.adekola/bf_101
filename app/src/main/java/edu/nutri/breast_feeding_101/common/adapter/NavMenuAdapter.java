package edu.nutri.breast_feeding_101.common.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.adapter.holder.NavMenuHolder;
import edu.nutri.breast_feeding_101.common.listener.ClickListener;

/**
 * Created by ribads on 4/2/18.
 */

public class NavMenuAdapter extends RecyclerView.Adapter<NavMenuHolder> {

    private List<String> nav_drawer_titles;
    private ClickListener<Integer> listener;
    Context context;

    public NavMenuAdapter(List<String> nav_drawer_titles, ClickListener<Integer> listener) {
        this.nav_drawer_titles = nav_drawer_titles;
        this.listener = listener;
    }

    @Override
    public NavMenuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.holder_nav_items, parent, false);
        return new NavMenuHolder(view);
    }

    @Override
    public void onBindViewHolder(NavMenuHolder holder, int position) {
        String title = nav_drawer_titles.get(position);
        TypedArray nav_drawer_icons = context.getResources().obtainTypedArray(R.array.nav_drawer_icons);

        int icon = nav_drawer_icons.getResourceId(position, -1);

        holder.bindData(title, icon);
        holder.itemView.setOnClickListener(v -> {
            listener.onClick(position);
        });
    }

    @Override
    public int getItemCount() {
        return nav_drawer_titles.size();
    }

    public void addAdminOption(List<String> tittleList) {
        nav_drawer_titles = tittleList;
//        nav_drawer_titles.add("admin");
        notifyDataSetChanged();
    }
}