package edu.nutri.breast_feeding_101.common.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.adapter.PickerAdapter;
import edu.nutri.breast_feeding_101.common.listener.OnItemClickListener;

/**
 * Created by Akano on 1/24/2018.
 */

public class PickerDialogFragment extends DialogFragment implements OnItemClickListener<Integer> {

    OnItemClickListener<Integer> clickListener;

    RecyclerView mRecyclerView;
    TextView mTitleView;
    boolean multiple;
    String title;
    ArrayList<String> items;
    List<String> selectedItems;
    PickerAdapter mPickerAdapter;

    public static PickerDialogFragment newInstance(String title, String[] array) {
        ArrayList<String> items = createList(array);
        return newInstance(title, items, null);
    }

    public static PickerDialogFragment newInstance(String title, ArrayList<String> items, String subTitle) {
        PickerDialogFragment fragment = new PickerDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putStringArrayList("items", items);
        if (subTitle != null)
            bundle.putString("sub", subTitle);
        fragment.setArguments(bundle);
        return fragment;
    }

    private static ArrayList<String> createList(String[] items) {
        return new ArrayList<>(Arrays.asList(items));
    }

    public void attachListener(OnItemClickListener<Integer> listener) {
            this.clickListener = listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.title = getArguments().getString("title");
            this.items = getArguments().getStringArrayList("items");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_picker_dialog, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        mRecyclerView = view.findViewById(R.id.recycler);
        mTitleView = view.findViewById(R.id.title);
        mTitleView.setText(this.title);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        if (!multiple)
            view.findViewById(R.id.multiple_area).setVisibility(View.GONE);
        if (!multiple)
            mPickerAdapter = new PickerAdapter(items, this);
        else
            mPickerAdapter = new PickerAdapter(items, selectedItems, this);
        mPickerAdapter.setMultiple(multiple);
        mRecyclerView.setAdapter(mPickerAdapter);
    }

    @Override
    public void onItemClick(Integer position) {
        dismiss();
        if (clickListener != null)
            clickListener.onItemClick(position);
    }
}