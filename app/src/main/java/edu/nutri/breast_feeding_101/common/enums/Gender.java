package edu.nutri.breast_feeding_101.common.enums;

/**
 * Created by ribads on 7/21/18.
 */
public enum Gender {

    Male, Female, Not_Specified;

    public static String[] getGenders() {
        int x = 0;
        String[] genderStrings = new String[Gender.values().length];
        for (Gender s : Gender.values()){
            genderStrings[x] = getGenderValue(s);
            x++;
        }
        return genderStrings;
    }

    public static String getGenderValue(Gender gender){

        switch (gender){
            case Male:
                return "Male";
            case Female:
                return "Female";
            case Not_Specified:
                return "Not_Specified";
            default:
                return "Error";
        }
    }

    public static Gender getGenderValue(String gender){

        switch (gender){
            case "Male":
                return Male;
            case "Female":
                return Female;
            case "Not_Specified":
                return Not_Specified;
        }
        return null;
    }

}
