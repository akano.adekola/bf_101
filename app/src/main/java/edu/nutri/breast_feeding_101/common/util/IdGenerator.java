package edu.nutri.breast_feeding_101.common.util;

import java.security.SecureRandom;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;

/**
 * Created by ribads on 6/24/18.
 */
public class IdGenerator {

    private String nextString() {
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = symbols[random.nextInt(symbols.length)];
        return new String(buf);
    }

    private static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private static final String lower = upper.toLowerCase(Locale.ROOT);

    private static final String digits = "0123456789";

    private static final String alphaNum = upper + lower + digits;

    private final Random random = Objects.requireNonNull(new SecureRandom());

    private final char[] symbols = alphaNum.toCharArray();

    private final char[] buf = new char[26];

    public String getDiscussionId(){
        return "DIS:"+nextString();
    }

    public String getCourseScoresId(){
        return "SCO:"+nextString();
    }

    public String getQuizLeaderBoardId(){
        return "QUI:"+nextString();
    }

    public String getChatMessageId(){
        return "CHA:"+nextString();
    }

    public String getFeedBackId() {
        return "FEE:"+nextString();
    }
}
