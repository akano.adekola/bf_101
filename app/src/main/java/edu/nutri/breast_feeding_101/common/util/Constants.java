package edu.nutri.breast_feeding_101.common.util;

/**
 * Created by Akano on 1/21/2018.
 */

public class Constants {

    public static int PASSWORD_MIN_LENGTH = 6;
    public static int USERNAME_MIN_LENGTH = 4;
    public static int ACCESS_CODE_LENGHT = 10;
    public static String IMAGE_CODE = "WC2783F2IC3DNC39";
    public static String PROFILE_IMAGE = "profileImages";
    public static String CHAT_IMAGE = "chatImages";

}
