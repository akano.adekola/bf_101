package edu.nutri.breast_feeding_101.common.enums;

/**
 * Created by ribads on 7/21/18.
 */
public enum NoOfChildren {

    No_child, one_child, two_children, three_children, four_children, More_than_four_children, Not_specified;

    public static String[] getNoOfChildren(){
        String[] nosOfChildren = new String[NoOfChildren.values().length];
        int x = 0;
        for (NoOfChildren nos: NoOfChildren.values()){
            nosOfChildren[x] = getNoOfChildrenValues(nos);
            x++;
        }
        return nosOfChildren;
    }

    private static String getNoOfChildrenValues(NoOfChildren nos) {
        switch (nos){
            case No_child:
                return "No child";
            case one_child:
                return "1 child";
            case two_children:
                return "2 children";
            case three_children:
                return "3 children";
            case four_children:
                return "4 children";
            case More_than_four_children:
                return "More than 4 children";
            case Not_specified:
                return "Not specified";
            default:
                return "Error";
        }
    }
}