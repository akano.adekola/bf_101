package edu.nutri.breast_feeding_101.dashboard.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.dashboard.adapter.holder.FaqViewHolder;

/**
 * Created by ribads on 6/3/18.
 */
public class FaqAdapter extends RecyclerView.Adapter<FaqViewHolder>{

    private String[] faq, faqAnswers;
    public FaqAdapter(String[] faq, String[] faqAnswers) {
        this.faq = faq;
        this.faqAnswers = faqAnswers;
    }

    @NonNull
    @Override
    public FaqViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_faq, parent, false);
        return new FaqViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FaqViewHolder holder, int position) {
        String question = faq[position];
        String answer = faqAnswers[position];
        holder.bindDate(question, answer, v -> {
            holder.toggleAnswer();
        });
    }

    @Override
    public int getItemCount() {
        return faq.length;
    }
}
