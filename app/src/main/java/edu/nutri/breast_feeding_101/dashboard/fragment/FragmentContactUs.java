package edu.nutri.breast_feeding_101.dashboard.fragment;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.util.IdGenerator;
import edu.nutri.breast_feeding_101.common.util.NotificationUtil;
import edu.nutri.breast_feeding_101.common.util.ValidatorUtil;
import edu.nutri.breast_feeding_101.common.util.ValueUtil;
import edu.nutri.breast_feeding_101.common.view.BaseFragment;
import edu.nutri.breast_feeding_101.database.models.FeedBack;
import edu.nutri.breast_feeding_101.database.models.Statistics;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;


/**
 * Created by Akano on 10/28/2016.
 */

public class FragmentContactUs extends BaseFragment implements View.OnClickListener{

    String phoneNo, email;
    EditText feedBackEt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View rootView = getFragmentView(inflater, R.layout.fragment_contact_us, container);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        phoneNo = getResources().getString(R.string.phone_no);
        email = getResources().getString(R.string.email_address);
        feedBackEt = rootView.findViewById(R.id.feed_back_et);
        rootView.findViewById(R.id.website_layout).setOnClickListener(this);
        rootView.findViewById(R.id.call_layout).setOnClickListener(this);
        rootView.findViewById(R.id.email_layout).setOnClickListener(this);
        rootView.findViewById(R.id.whatsapp_layout).setOnClickListener(this);
        rootView.findViewById(R.id.send_img).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.send_img:
                // TODO: 6/3/18 setError() not working
                if (ValidatorUtil.isNotEmpty(feedBackEt))
                    sendFeedBack(ValueUtil.getText(feedBackEt));
                break;
            case R.id.call_layout:
                makeCall();
                break;
            case R.id.email_layout:
                sendEmail();
                break;
            case R.id.website_layout:
                openWebsite();
                break;
            case R.id.whatsapp_layout:
                sendWhatsappMessage();
                break;
        }
    }

    private void sendWhatsappMessage() {
        Intent sendIntent = new Intent(Intent.ACTION_SENDTO,Uri.parse("smsto:" + "" + phoneNo + "?body=" + ""));
        sendIntent.setPackage("com.whatsapp");
        startActivity(sendIntent);
    }

    private void sendFeedBack(String message) {
        FeedBack feedBack = new FeedBack();
        feedBack.setMessage(message);
        feedBack.setCreatorId(user.getUserId());
        feedBack.setCreatorName(user.getFirstName());
        feedBack.setId(new IdGenerator().getFeedBackId());
        baseDatabaseReference.child(FeedBack.tableName).push().setValue(feedBack);
        updateFeedBackCount();
        ValueUtil.clearText(feedBackEt);
        NotificationUtil.showSuccessDialog(getFragmentManager(), "Success", "Thank you for your feedback");
    }

    private void updateFeedBackCount() {
        baseDatabaseReference.child(Statistics.table_name).runTransaction(new Transaction.Handler() {
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                Statistics statistics = mutableData.getValue(Statistics.class);
                statistics.updateFeedbackCount();
                mutableData.setValue(statistics);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {
            }
        });
    }

    private void openWebsite() {
    }

    private void sendEmail() {
        Intent mailIntent = new Intent(Intent.ACTION_SEND);
        mailIntent.setType("text/plain");
//        mailIntent.setType("message/rfc822");
        mailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        startActivity(Intent.createChooser(mailIntent, "send mail"));
    }

    private void makeCall() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:"+phoneNo));

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, CALL_PHONE_PERMISSION_REQUEST_CODE);
        }
        else
            startActivity(callIntent);
    }
    int CALL_PHONE_PERMISSION_REQUEST_CODE = 873;
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CALL_PHONE_PERMISSION_REQUEST_CODE){
            if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED){
                makeCall();
            }
        }
    }
}
