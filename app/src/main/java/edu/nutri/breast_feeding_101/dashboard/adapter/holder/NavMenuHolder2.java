package edu.nutri.breast_feeding_101.dashboard.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import edu.nutri.breast_feeding_101.R;

/**
 * Created by ribads on 4/2/18.
 */

public class NavMenuHolder2 extends RecyclerView.ViewHolder {
    TextView titileTv;
    public NavMenuHolder2(View itemView) {
        super(itemView);
        titileTv = itemView.findViewById(R.id.title_tv);

    }

    public void bindData(String title) {
        this.titileTv.setText(title);
    }
}
