package edu.nutri.breast_feeding_101.dashboard.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.dashboard.adapter.holder.CourseOutlineHolder;

/**
 * Created by ribads on 5/30/18.
 */
public class CourseOutlineAdapter extends RecyclerView.Adapter<CourseOutlineHolder>{

    String[] courseOutline;

    public CourseOutlineAdapter(String[] courseOutline) {
        this.courseOutline = courseOutline;
    }

    @NonNull
    @Override
    public CourseOutlineHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_course_outline, parent, false);
        return new CourseOutlineHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CourseOutlineHolder holder, int position) {
        String outlineItem = courseOutline[position];
        holder.bindDate(outlineItem);
    }

    @Override
    public int getItemCount() {
        return courseOutline.length;
    }
}
