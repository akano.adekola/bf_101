package edu.nutri.breast_feeding_101.dashboard;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.view.BaseFragment;
import edu.nutri.breast_feeding_101.dashboard.adapter.FaqAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class FaqFragment extends BaseFragment {

    RecyclerView faqRecyclerView;
    FaqAdapter faqAdapter;
    public FaqFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = getFragmentView(inflater, R.layout.fragment_faq, container);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        String[] faq = getResources().getStringArray(R.array.faq);
        String[] faqAnswers = getResources().getStringArray(R.array.faq_answers);
        faqRecyclerView = view.findViewById(R.id.faq_recycler);
        faqAdapter = new FaqAdapter(faq, faqAnswers);
        faqRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        faqRecyclerView.setAdapter(faqAdapter);
    }
}