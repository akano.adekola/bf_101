package edu.nutri.breast_feeding_101.dashboard.fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.enums.Enums;
import edu.nutri.breast_feeding_101.common.enums.Gender;
import edu.nutri.breast_feeding_101.common.fragment.PickerDialogFragment;
import edu.nutri.breast_feeding_101.common.listener.FirebaseUserDataListener;
import edu.nutri.breast_feeding_101.common.listener.OnItemClickListener;
import edu.nutri.breast_feeding_101.common.util.Constants;
import edu.nutri.breast_feeding_101.common.util.NotificationUtil;
import edu.nutri.breast_feeding_101.common.view.BaseFragment;
import edu.nutri.breast_feeding_101.dashboard.GlideApp;
import edu.nutri.breast_feeding_101.dashboard.interactor.ProfileInteractor;
import edu.nutri.breast_feeding_101.database.Data;
import edu.nutri.breast_feeding_101.database.models.User;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends BaseFragment implements View.OnClickListener, FirebaseUserDataListener {

    private ProfileInteractor interactor;
    private String[] options = null;
    private String title = "";
    ImageView imageView;
    Calendar birthCalendar;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return getFragmentView(inflater, R.layout.fragment_profile, container, this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageView = view.findViewById(R.id.profile_img);
        interactor = new ProfileInteractor(view, this, usersDatabaseReference, baseDatabaseReference);
        storageReference = storageReference.child(Constants.PROFILE_IMAGE).child(userId);
        showImage();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.dob_et)
            setDob();
        else if (id == R.id.profile_img){
            getImage();
        }
        else {
            switch (id) {
                case R.id.gender_et:
                    title = "Select your gender";
                    options = Gender.getGenders();
                    break;
                case R.id.children_et:
                    title = "Select the number of your children";
                    options = Enums.getNoOfChildren();
                    break;
                case R.id.marital_status_et:
                    title = "Select your Marital status";
                    options = Enums.getMaritalStatus();
                    break;
                case R.id.level_of_education_et:
                    title = "Select your level of education";
                    options = Enums.getLevelOfEducation();
                    break;
            }

            PickerDialogFragment pickerDialogFragment = PickerDialogFragment.newInstance(title, options);
            pickerDialogFragment.attachListener(integer -> {
                switch (id) {
                    case R.id.gender_et:
                        interactor.setGender(options[integer]);
                        break;
                    case R.id.children_et:
                        interactor.setChildren(options[integer]);
                        break;
                    case R.id.marital_status_et:
                        interactor.setMaritalStatus(options[integer]);
                        break;
                    case R.id.level_of_education_et:
                        interactor.setLevelOfEducation(options[integer]);
                        break;
                }
                interactor.update();
            });
            pickerDialogFragment.show(getFragmentManager(), null);
        }
    }

    private void getImage() {
        CropImage.activity()
                .setAspectRatio(1, 1)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(getContext(), this);
    }

    private void setDob() {
        int defaultYear = 1990;
        int defaultMonth = 0;
        int defaultDay = 1;
        if (birthCalendar != null){
            defaultYear = birthCalendar.get(Calendar.YEAR);
            defaultMonth = birthCalendar.get(Calendar.MONTH);
            defaultDay = birthCalendar.get(Calendar.DAY_OF_MONTH);
        }
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), (view, year, month, dayOfMonth) -> {
            Calendar dobCal = Calendar.getInstance();
            dobCal.set(Calendar.YEAR, year);
            dobCal.set(Calendar.MONTH, month);
            dobCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            interactor.setDob(dobCal);
        }, defaultYear, defaultMonth, defaultDay);
        datePickerDialog.setTitle("Set your date of birth");
        datePickerDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                uploadImage(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    // TODO: 6/29/18  save image locally and remotely and fetch from local
    private void uploadImage(Uri resultUri) {
        UploadTask uploadTask = storageReference.putFile(resultUri);
        showProgress("Uploading...");
        // TODO: 7/18/18 replace toast with dialogs
        uploadTask.addOnFailureListener(exception -> {
            NotificationUtil.showToast(getActivity(), " Upload failed");
        })
        .addOnSuccessListener(taskSnapshot -> {
            NotificationUtil.showToast(getActivity(), "Upload successful");
            hideProgress();
            showImage();
        })
        .addOnProgressListener(taskSnapshot -> {
            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
            updateProgressDialog("Upload is " + (int)progress + "% done");
        });
    }

    private void showImage() {
        GlideApp.with(this)
                .load(storageReference)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.account_circle_black)
                .placeholder(R.drawable.account_circle_black)
                .into(imageView);
    }

    @Override
    public void onUserDataFetch(User user) {
        interactor.setUserDetails(user);
        if (user.getDateOfBirth() != null) {
            birthCalendar = Calendar.getInstance();
            birthCalendar.setTime(new Date(user.getDateOfBirth()));
        }
    }
}