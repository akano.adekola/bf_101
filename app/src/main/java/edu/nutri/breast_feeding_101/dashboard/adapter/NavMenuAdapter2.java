package edu.nutri.breast_feeding_101.dashboard.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.adapter.holder.NavMenuHolder;
import edu.nutri.breast_feeding_101.common.listener.ClickListener;
import edu.nutri.breast_feeding_101.dashboard.adapter.holder.NavMenuHolder2;

/**
 * Created by ribads on 4/2/18.
 */

public class NavMenuAdapter2 extends RecyclerView.Adapter<NavMenuHolder2> {

    String[] nav_drawer_titles;
    ClickListener listener;

    public NavMenuAdapter2(String[] nav_drawer_titles, ClickListener listener) {
        this.nav_drawer_titles = nav_drawer_titles;
        this.listener = listener;
    }

    @Override
    public NavMenuHolder2 onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_nav_items2, parent, false);

        return new NavMenuHolder2(view);
    }

    @Override
    public void onBindViewHolder(NavMenuHolder2 holder, int position) {

        String title = nav_drawer_titles[position];
        holder.bindData(title);
        holder.itemView.setOnClickListener(v -> {
            listener.onClick(position);
        });
    }

    @Override
    public int getItemCount() {
        return nav_drawer_titles.length;
    }
}
