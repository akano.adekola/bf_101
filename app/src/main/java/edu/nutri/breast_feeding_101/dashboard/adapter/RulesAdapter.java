package edu.nutri.breast_feeding_101.dashboard.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.dashboard.adapter.holder.RulesHolder;

/**
 * Created by ribads on 6/3/18.
 */
public class RulesAdapter extends RecyclerView.Adapter<RulesHolder>{
    String[] rules;
    public RulesAdapter(String[] rules) {
        this.rules = rules;
    }

    @NonNull
    @Override
    public RulesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_rules, parent, false);
        return new RulesHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RulesHolder holder, int position) {
        String rule = rules[position];
        holder.bindData(rule);
    }

    @Override
    public int getItemCount() {
        return rules.length;
    }
}
