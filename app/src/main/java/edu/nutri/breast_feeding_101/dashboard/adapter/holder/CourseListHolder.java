package edu.nutri.breast_feeding_101.dashboard.adapter.holder;

import android.opengl.Visibility;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.listener.ClickListener;
import edu.nutri.breast_feeding_101.dashboard.adapter.CourseOutlineAdapter;

/**
 * Created by ribads on 4/20/18.
 */

public class CourseListHolder extends RecyclerView.ViewHolder {

    private TextView courseTitleTv, courseDescriptionTv;
    private ImageView courseImg;
    private Button startBtn;
    private RecyclerView courseOutlineRecycler;
    private LinearLayout outLineLayout;
    private CourseOutlineAdapter courseOutlineAdapter;
    public CourseListHolder(View itemView) {
        super(itemView);
        outLineLayout = itemView.findViewById(R.id.outline_layout);
        courseTitleTv = itemView.findViewById(R.id.title_tv);
        courseDescriptionTv = itemView.findViewById(R.id.description_tv);
        courseImg = itemView.findViewById(R.id.course_img);
        startBtn = itemView.findViewById(R.id.start_btn);
        courseOutlineRecycler = itemView.findViewById(R.id.course_outline_recycler);
    }

    public void bindData(String title, String description, int icon, int position, ClickListener listener) {
        itemView.setOnClickListener(v -> toggleOutline());
        startBtn.setOnClickListener(v -> listener.onClick(position));

        List<String> outlineList = new ArrayList<>();
        outlineList.add("Pre Assessment");
        outlineList.add("Introduction");
        outlineList.addAll(Arrays.asList(itemView.getResources().getStringArray(getCourseOutline(position+1))));
        outlineList.add("Summary");
        outlineList.add("Post Assessment");

        String[] courseOutline = new String[outlineList.size()];
        courseOutline = outlineList.toArray(courseOutline);

        courseOutlineAdapter = new CourseOutlineAdapter(courseOutline);
        courseOutlineRecycler.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
        courseOutlineRecycler.setAdapter(courseOutlineAdapter);

        courseTitleTv.setText((position+1)+") "+ title);
        courseDescriptionTv.setText(description);
//        courseImg.setImageResource(icon);
    }

    private void toggleOutline() {
        int visibility = outLineLayout.getVisibility();
        if (visibility == View.GONE)
            outLineLayout.setVisibility(View.VISIBLE);
        else
            outLineLayout.setVisibility(View.GONE);
    }

    private int getCourseOutline(int courseNo) {
        int array = 0;
        switch (courseNo){
            case 1:
                array = R.array.course_1_sub_titles;
                break;
            case 2:
                array = R.array.course_2_sub_titles;
                break;
            case 3:
                array = R.array.course_3_sub_titles;
                break;
            case 4:
                array = R.array.course_4_sub_titles;
                break;
            case 5:
                array = R.array.course_5_sub_titles;
                break;
            case 6:
                array = R.array.course_6_sub_titles;
                break;
        }
        return array;
    }

}
