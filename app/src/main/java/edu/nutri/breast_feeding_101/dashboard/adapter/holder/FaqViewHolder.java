package edu.nutri.breast_feeding_101.dashboard.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import edu.nutri.breast_feeding_101.R;

/**
 * Created by ribads on 6/3/18.
 */
public class FaqViewHolder extends RecyclerView.ViewHolder {
    TextView questionTv, answerTv;
    ImageView plusImg;
    LinearLayout answerLayout;
    public FaqViewHolder(View itemView) {
        super(itemView);
        questionTv = itemView.findViewById(R.id.question_tv);
        answerTv = itemView.findViewById(R.id.answer_tv);
        plusImg = itemView.findViewById(R.id.plus_img);
        answerLayout = itemView.findViewById(R.id.answer_layout);
    }

    public void bindDate(String question, String answer, View.OnClickListener listener) {
        questionTv.setText(question);
        answerTv.setText(answer);
        itemView.setOnClickListener(v -> {
            listener.onClick(v);
        });
    }

    public void toggleAnswer() {
        if (answerLayout.getVisibility() == View.VISIBLE) {
            answerLayout.setVisibility(View.GONE);
            questionTv.setTextColor(itemView.getResources().getColor(R.color.black));
            plusImg.setImageResource(R.drawable.plus);
        }
        else {
            answerLayout.setVisibility(View.VISIBLE);
            questionTv.setTextColor(itemView.getResources().getColor(R.color.colorPrimary));
            plusImg.setImageResource(R.drawable.minus);
        }
    }
}