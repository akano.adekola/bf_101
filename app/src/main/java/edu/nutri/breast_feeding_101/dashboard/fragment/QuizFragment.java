package edu.nutri.breast_feeding_101.dashboard.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.util.NotificationUtil;
import edu.nutri.breast_feeding_101.common.view.BaseFragment;
import edu.nutri.breast_feeding_101.courses.view.CourseAssessmentActivity;
import edu.nutri.breast_feeding_101.courses.view.QuizLeaderBoardActivity;
import edu.nutri.breast_feeding_101.dashboard.adapter.RulesAdapter;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Akano on 12/8/2016.
 */
public class QuizFragment extends BaseFragment{

    RecyclerView rulesRecycler;
    RulesAdapter rulesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View rootView = getFragmentView(inflater, R.layout.fragment_quiz, container);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        rootView.findViewById(R.id.start_btn).setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putInt("courseNo", 0);
            Intent intent = new Intent(getActivity(), CourseAssessmentActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, 382);
        });

        rootView.findViewById(R.id.leader_board_layout).setOnClickListener(v -> {
            Bundle bundle = new Bundle();
//            bundle.putInt("quizScore", 320);
            Intent intent = new Intent(getActivity(), QuizLeaderBoardActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, 382);
        });

        String[] rules = getResources().getStringArray(R.array.rules);
        rulesRecycler = rootView.findViewById(R.id.rules_layout);
        rulesAdapter = new RulesAdapter(rules);
        rulesRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        rulesRecycler.setAdapter(rulesAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}