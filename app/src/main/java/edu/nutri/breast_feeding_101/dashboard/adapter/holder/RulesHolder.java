package edu.nutri.breast_feeding_101.dashboard.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import edu.nutri.breast_feeding_101.R;

/**
 * Created by ribads on 6/3/18.
 */
public class RulesHolder extends RecyclerView.ViewHolder {
    TextView ruleTv;
    public RulesHolder(View itemView) {
        super(itemView);
        ruleTv = itemView.findViewById(R.id.rule_tv);
    }

    public void bindData(String rule) {
        ruleTv.setText(""+rule);
    }
}
