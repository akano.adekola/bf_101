package edu.nutri.breast_feeding_101.dashboard.view;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.storage.FirebaseStorage;

import java.util.Arrays;
import java.util.List;

import edu.nutri.breast_feeding_101.admin.fragment.AdminFragment;
import edu.nutri.breast_feeding_101.common.listener.FirebaseUserDataListener;
import edu.nutri.breast_feeding_101.common.util.Constants;
import edu.nutri.breast_feeding_101.common.util.NotificationUtil;
import edu.nutri.breast_feeding_101.community.fragment.CommunityFragment;
import edu.nutri.breast_feeding_101.dashboard.GlideApp;
import edu.nutri.breast_feeding_101.dashboard.fragment.FragmentContactUs;
import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.adapter.NavMenuAdapter;
import edu.nutri.breast_feeding_101.common.listener.ClickListener;
import edu.nutri.breast_feeding_101.common.util.LauncherUtil;
import edu.nutri.breast_feeding_101.common.view.BaseActivity;
import edu.nutri.breast_feeding_101.dashboard.FaqFragment;
import edu.nutri.breast_feeding_101.dashboard.adapter.NavMenuAdapter2;
import edu.nutri.breast_feeding_101.courses.fragment.CourseListFragment;
import edu.nutri.breast_feeding_101.dashboard.fragment.ProfileFragment;
import edu.nutri.breast_feeding_101.dashboard.fragment.QuizFragment;
import edu.nutri.breast_feeding_101.database.models.User;

public class MainActivity extends BaseActivity implements ClickListener<Integer>, FirebaseUserDataListener {

	ImageView profileImg;
	TextView emailTv, nameTv;
	RecyclerView recycler, recycler2;
	DrawerLayout drawerLayout;
	Toolbar toolbar;
	NavMenuAdapter navMenuAdapter;
	NavMenuAdapter2 navMenuAdapter2;
	int frame;
    List<String> tittleList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		attachFirebaseUserListener(this);
		setContentView(R.layout.activity_main);
        frame = R.id.frame;
		initViews();
	}

	private void initViews() {
		toolbar = findViewById(R.id.tool_bar);
		profileImg = findViewById(R.id.profile_img);
		emailTv = findViewById(R.id.email_tv);
		nameTv = findViewById(R.id.name_tv);
		recycler = findViewById(R.id.nav_menu_recycler);
		recycler2 = findViewById(R.id.nav_menu2_recycler);
		drawerLayout = findViewById(R.id.drawer_layout);

		setSupportActionBar(toolbar);

		android.support.v7.app.ActionBarDrawerToggle actionBarDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, drawerLayout,
				toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawerLayout.addDrawerListener(actionBarDrawerToggle);
		actionBarDrawerToggle.syncState();

//		String[] tittleList = getResources().getStringArray(R.array.nav_drawer_titles);
        tittleList = Arrays.asList(getResources().getStringArray(R.array.nav_drawer_titles));
        String[] tittleList2 = getResources().getStringArray(R.array.nav_drawer_titles2);

		navMenuAdapter = new NavMenuAdapter(tittleList, this);
		navMenuAdapter2 = new NavMenuAdapter2(tittleList2, (ClickListener<Integer>) position -> {
            showSubMenuFragment(position);
			toggleDrawer();
        });

		recycler2.setLayoutManager(new LinearLayoutManager(this));
		recycler2.setAdapter(navMenuAdapter2);

		recycler.setLayoutManager(new LinearLayoutManager(this));
		recycler.setAdapter(navMenuAdapter);

		//default fragment
		LauncherUtil.launchFragment(getSupportFragmentManager(), frame, getFragment(0), true );
		drawerLayout.openDrawer(Gravity.START);

		setProfileImage();
	}

    private void setProfileImage() {
        storageReference = FirebaseStorage.getInstance().getReference().child(Constants.PROFILE_IMAGE).child(userId);

        GlideApp.with(this)
                .load(storageReference)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.account_circle)
                .placeholder(R.drawable.account_circle)
                .into(profileImg);
    }

    private void showSubMenuFragment(Integer position) {
		if (position == 2)
			logOut(this);
		else
        	LauncherUtil.launchFragment(getSupportFragmentManager(), frame, getSubMenuFragment(position), true );
    }

    @Override
	public void onClick(Integer position) {
		LauncherUtil.launchFragment(getSupportFragmentManager(), frame, getFragment(position), true );
		toggleDrawer();
	}

    private Fragment getSubMenuFragment(Integer position) {
        switch (position){
            case 0:
                return new FaqFragment();
            case 1:
                return new FragmentContactUs();
        }
        return null;
    }

	private Fragment getFragment(Integer position) {
		switch (position){
			case 0:
				return new ProfileFragment();
			case 1:
				return new CourseListFragment();
			case 2:
				return new QuizFragment();
			case 3:
				return new CommunityFragment();
			case 4:
				return new ProfileFragment();
			case 5:
				return new AdminFragment();
            case 6:
                return new ProfileFragment();
            default:
                return new ProfileFragment();
		}
    }

	private void toggleDrawer() {
		if (drawerLayout.isDrawerOpen(Gravity.START))
			drawerLayout.closeDrawer(Gravity.START);
	}

	@Override
	public void onBackPressed() {
		if (drawerLayout.isDrawerOpen(Gravity.START))
			drawerLayout.closeDrawer(Gravity.START);
		else
			super.onBackPressed();
	}

	@Override
	public void onUserDataFetch(User user) {
		String name = user.getFirstName() != null ? user.getFirstName() : user.getLastName();
		nameTv.setText(name);
		emailTv.setText(user.getEmail());
		if (user.isAdmin()) {
			tittleList = Arrays.asList(getResources().getStringArray(R.array.admin_nav_drawer_titles));
			navMenuAdapter.addAdminOption(tittleList);
		}
	}
}