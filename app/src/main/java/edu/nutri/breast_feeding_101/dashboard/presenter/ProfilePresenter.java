package edu.nutri.breast_feeding_101.dashboard.presenter;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.util.ValueUtil;

/**
 * Created by ribads on 4/12/18.
 */

public class ProfilePresenter {

    private EditText firstNameEt, lastNameEt, emailEt, genderEt, childrenEt, dobEt, levelOfEducationEt, maritalStatusEt;

    public ProfilePresenter(View view, View.OnClickListener listener) {
        firstNameEt = view.findViewById(R.id.first_name_et);
        lastNameEt = view.findViewById(R.id.last_name_et);
        emailEt = view.findViewById(R.id.email_et);
        genderEt = view.findViewById(R.id.gender_et);
        genderEt.setOnClickListener(listener);
        childrenEt = view.findViewById(R.id.children_et);
        childrenEt.setOnClickListener(listener);
        dobEt = view.findViewById(R.id.dob_et);
        dobEt.setOnClickListener(listener);
        maritalStatusEt = view.findViewById(R.id.marital_status_et);
        maritalStatusEt.setOnClickListener(listener);
        levelOfEducationEt = view.findViewById(R.id.level_of_education_et);
        levelOfEducationEt.setOnClickListener(listener);
        ImageView profileImage = view.findViewById(R.id.profile_img);
        profileImage.setOnClickListener(listener);
    }

     public void setFirstName(String firstName){
         firstNameEt.setText(firstName);
     }

     public String getFirstname(){
        return ValueUtil.getText(firstNameEt);
     }

     public void setLastName(String lastName){
         lastNameEt.setText(lastName);
     }

    public String getLastname(){
        return ValueUtil.getText(lastNameEt);
    }

    public void setEmail(String email){
         emailEt.setText(email);
    }

    public void setGender(String gender){
         genderEt.setText(gender);
    }

    public String getGender(){
        return ValueUtil.getText(genderEt);
    }

    public void setChildren(String children){
         childrenEt.setText(children);
    }

    public String getChildren(){
        return ValueUtil.getText(childrenEt);
    }

    public void setDob(String dob){
         dobEt.setText(dob);
    }

    public String getDob(){
        return ValueUtil.getText(dobEt);
    }

    public void setMaritalStatus(String maritalStatus){
        maritalStatusEt.setText(maritalStatus);
    }

    public String getMaritalStatus(){
        return ValueUtil.getText(maritalStatusEt);
    }

    public void setLevelOfEducation(String levelOfEducation){
        levelOfEducationEt.setText(levelOfEducation);
    }

    public String getLevelOfEducation(){
        return ValueUtil.getText(levelOfEducationEt);
    }
}
