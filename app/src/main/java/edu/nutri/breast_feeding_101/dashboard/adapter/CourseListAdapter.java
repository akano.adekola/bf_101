package edu.nutri.breast_feeding_101.dashboard.adapter;

import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.listener.ClickListener;
import edu.nutri.breast_feeding_101.dashboard.adapter.holder.CourseListHolder;

/**
 * Created by ribads on 4/20/18.
 */

public class CourseListAdapter extends RecyclerView.Adapter<CourseListHolder> {

    private String[] courseTitles, courseDescription;
    private TypedArray courseIcons;
    private ClickListener listener;

    @NonNull
    @Override
    public CourseListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_course_list, parent, false);
        return new CourseListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CourseListHolder holder, int position) {
        String title = courseTitles[position];
        String description = courseDescription[position];
        int icon = courseIcons.getResourceId(position, -1);
        holder.bindData(title, description, icon, position, listener);
    }

    @Override
    public int getItemCount() {
        return courseTitles.length;
    }

    public void setData(String[] courseTitles, String[] courseDescription, TypedArray courseIcons) {
        this.courseTitles = courseTitles;
        this.courseDescription = courseDescription;
        this.courseIcons = courseIcons;
    }

    public void setListener(ClickListener listener) {
        this.listener = listener;
    }
}
