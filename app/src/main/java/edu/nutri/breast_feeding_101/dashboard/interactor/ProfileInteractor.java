package edu.nutri.breast_feeding_101.dashboard.interactor;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import edu.nutri.breast_feeding_101.common.enums.Enums;
import edu.nutri.breast_feeding_101.common.enums.Gender;
import edu.nutri.breast_feeding_101.common.enums.LevelOfEducation;
import edu.nutri.breast_feeding_101.common.enums.MaritalStatus;
import edu.nutri.breast_feeding_101.common.enums.NoOfChildren;
import edu.nutri.breast_feeding_101.common.util.NotificationUtil;
import edu.nutri.breast_feeding_101.common.util.ValueUtil;
import edu.nutri.breast_feeding_101.dashboard.presenter.ProfilePresenter;
import edu.nutri.breast_feeding_101.database.models.Statistics;
import edu.nutri.breast_feeding_101.database.models.User;

/**
 * Created by ribads on 4/12/18.
 */

public class ProfileInteractor {

    private ProfilePresenter presenter;
    private User user;
    private DatabaseReference usersDatabaseReference, baseDatabaseReference;
    private Context context;

    public ProfileInteractor(View view, View.OnClickListener listener, DatabaseReference usersDatabaseReference, DatabaseReference baseDatabaseReference) {
        presenter = new ProfilePresenter(view, listener);
        context = view.getContext();
        this.usersDatabaseReference = usersDatabaseReference;
        this.baseDatabaseReference = baseDatabaseReference;
    }

    public void setUserDetails(User user) {
        this.user = user;
        presenter.setDob(user.getDateOfBirth("YYYY - MM - dd"));
        presenter.setFirstName(user.getFirstName());
        presenter.setLastName(user.getLastName());
        presenter.setEmail(user.getEmail());
        presenter.setChildren(user.getNoOfChildren());
        presenter.setGender(user.getGender());
        presenter.setLevelOfEducation(user.getLevelOfEducation());
        presenter.setMaritalStatus(user.getMaritalStatus());
    }

    public void setGender(String option) {
        presenter.setGender(option);
    }

    public void setLevelOfEducation(String option) {
        presenter.setLevelOfEducation(option);
    }

    public void setMaritalStatus(String option) {
        presenter.setMaritalStatus(option);
    }

    public void setChildren(String option) {
        presenter.setChildren(option);
    }

    private Date dobDate;
    public void setDob(Calendar dobCal) {
        dobDate = dobCal.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY - MM - dd");
        presenter.setDob(simpleDateFormat.format(dobDate));
        update();
    }

    private String firstName, lastName, dateOfBirth, levelOfEducation, maritalStatus, noOfChildren, gender;

    public void update() {
        firstName = presenter.getFirstname();
        lastName = presenter.getLastname();
        dateOfBirth = presenter.getDob();
        levelOfEducation = presenter.getLevelOfEducation();
        maritalStatus = presenter.getMaritalStatus();
        noOfChildren = presenter.getChildren();
        gender = presenter.getGender();

        setStatistics(user, baseDatabaseReference);

        user.setFirstName(firstName);
        user.setLastName(lastName);
        if (dobDate !=null)
            user.setDateOfBirth(dobDate.getTime());
        user.setLevelOfEducation(levelOfEducation);
        user.setMaritalStatus(maritalStatus);
        user.setNoOfChildren(noOfChildren);
        user.setGender(gender);
        usersDatabaseReference.child(user.getUserId()).setValue(user);
    }

    private void setStatistics(User user, DatabaseReference baseDatabaseReference) {
        String userGender = user.getGender();
        String userLevelOfEducation = user.getLevelOfEducation();
        String userMaritalStatus = user.getMaritalStatus();
        String userNoOfChildren = user.getNoOfChildren();

//        NotificationUtil.showToast(context, gender + "--");
        baseDatabaseReference.child(Statistics.table_name).runTransaction(new Transaction.Handler() {
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                Statistics statistics = mutableData.getValue(Statistics.class);
                statistics.updateGenderCount(Gender.valueOf(userGender), Gender.valueOf(gender));
                statistics.updateLevelOfEducationCount(LevelOfEducation.valueOf(userLevelOfEducation), LevelOfEducation.valueOf(levelOfEducation));
                statistics.updateMaritalStatusCount(MaritalStatus.valueOf(userMaritalStatus), MaritalStatus.valueOf(maritalStatus));
                statistics.updateNoOfChildrenCount(NoOfChildren.valueOf(userNoOfChildren), NoOfChildren.valueOf(noOfChildren));
                mutableData.setValue(statistics);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {

            }
        });
    }
}