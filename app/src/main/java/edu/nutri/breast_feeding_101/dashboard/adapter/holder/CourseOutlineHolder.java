package edu.nutri.breast_feeding_101.dashboard.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import edu.nutri.breast_feeding_101.R;

/**
 * Created by ribads on 5/30/18.
 */
public class CourseOutlineHolder extends RecyclerView.ViewHolder {

    TextView itemTv;

    public CourseOutlineHolder(View itemView) {
        super(itemView);
        itemTv = itemView.findViewById(R.id.item_tv);
    }

    public void bindDate(String outlineItem) {
        itemTv.setText(outlineItem);
    }
}
