package edu.nutri.breast_feeding_101.example.view;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.view.BaseActivity;
import edu.nutri.breast_feeding_101.database.models.User;

public class ExampleActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example);

        start();
    }

    private void start() {
            database.getReference().child("Users").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                Map map = dataSnapshot.getValue(Map.class);

                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        Map<String, Object> map = (Map<String, Object>) dataSnapshot1.getValue();
                        User user = new User();

                        String name = map.get("Name").toString();

                        String[] words = name.split("\\s+");

                        if (words.length > 1){
                            user.setLastName(words[0]);
                            user.setFirstName(words[1]);
                        }
                        else
                            user.setFirstName(name);

                        user.setGender(map.get("Gender").toString());
                        user.setEmail(map.get("Email").toString());
                        user.setLevelOfEducation(map.get("Level_of_Education").toString());
                        user.setMaritalStatus(map.get("Marital_Status").toString());
                        user.setNoOfChildren(map.get("No_of_Children").toString());
                        user.setReligion(map.get("Religion").toString());
                        user.setPregnancyStatus(map.get("Pregnancy_Status").toString());
                        user.setUserId(map.get("User_id").toString());

                        usersDatabaseReference.child(map.get("User_id").toString()).setValue(user);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

    }
}
