package edu.nutri.breast_feeding_101.database.models;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import edu.nutri.breast_feeding_101.common.util.NotificationUtil;

/**
 * Created by ribads on 6/24/18.
 */
class BaseModel {

    private String id;
    private Long createdDate;
    private String creatorName;
    private String creatorId;


    public void setId(String id){
        this.id = id;
        setCreatedDate();
    }

    public String getId(){
        return id;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    private void setCreatedDate() {
        this.createdDate = Calendar.getInstance().getTime().getTime();
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }
}
