package edu.nutri.breast_feeding_101.database.models;

/**
 * Created by ribads on 6/3/18.
 */
public class QuizLeaderBoard extends BaseModel{

    public void Quizleaderboard(){
    }

    public static String tableName = "QuizLeaderBoard";
    private Long score;

    public void setScore(int score){
        this.score = (long) score;
    }

    public int getScore(){
        return score.intValue();
    }

}
