package edu.nutri.breast_feeding_101.database.models;

import edu.nutri.breast_feeding_101.common.enums.Enums;
import edu.nutri.breast_feeding_101.common.enums.Gender;
import edu.nutri.breast_feeding_101.common.enums.LevelOfEducation;
import edu.nutri.breast_feeding_101.common.enums.MaritalStatus;
import edu.nutri.breast_feeding_101.common.enums.NoOfChildren;
import edu.nutri.breast_feeding_101.common.util.ValidatorUtil;

/**
 * Created by ribads on 7/6/18.
 */
public class Statistics {

    public static String table_name = "Statistics";

    public Long femaleCount;
    public Long maleCount;
    public Long genderNotSpecifiedCount;

    public Long noFormalEducationCount;
    public Long primaryEducationCount;
    public Long secondaryEducationCount;
    public Long tertiaryEducationCount;
    public Long educationNotSpecifiedCount;

    public Long singleCount;
    public Long marriedCount;
    public Long maritalStatusNotSpecifiedCount;

    public Long ageBelow20YearsCount;
    public Long age21to30YearsCount;
    public Long age31to40YearsCount;
    public Long ageabove40YearsCount;

    public Long totalDiscussionCount;
    public Long totalUsersCount;
    public Long totallikesCount;

    public Long totalCourseCount;
    public Long totalCourseFailedCount;
    public Long totalCoursePassedCount;
    public Long totalCourse1Count;
    public Long totalCourse2Count;
    public Long totalCourse3Count;
    public Long totalCourse4Count;
    public Long totalCourse5Count;
    public Long totalCourse6Count;
    public Long totalAssessmentScoreCount;
    public Long totalPreAssessmentScoreCount;
    public Long totalQuizCount;
    public Long totalQuizScoreCount;

    public Long totalChatMessagesCount;
    public Long totalImagesCount;
    public Long totalTextCount;

    public Long totalFeedbackCount;

    public void updateDiscussionCount() {
        totalDiscussionCount += 1;
    }

    public void updateGenderCount() {
        updateFemaleCount(1);
    }

    public void updateGenderCount(Gender previousGender, Gender newGender){
//        updateMaleCount(1);

        switch (newGender){
            case Male:
                updateMaleCount(1);
                break;
            case Female:
                updateFemaleCount(1);
                break;
            case Not_Specified:
                updatGenderNotSpecifiedCount(1);
                break;
        }

        if (previousGender != null)
            switch (previousGender){
                case Male:
                    updateMaleCount(-1);
                    break;
                case Female:
                    updateFemaleCount(-1);
                    break;
                case Not_Specified:
                    updatGenderNotSpecifiedCount(-1);
                    break;
            }
    }

    private void updateFemaleCount(int count){
        femaleCount += count;
    }

    private void updateMaleCount(int count){
        maleCount += count;
    }

    private void updatGenderNotSpecifiedCount(int count){
        genderNotSpecifiedCount += count;
    }

    public void updateLevelOfEducationCount(LevelOfEducation levelOfEducation, LevelOfEducation levelOfEducation1) {
    }

    public void updateMaritalStatusCount(MaritalStatus maritalStatus, MaritalStatus maritalStatus1) {
    }

    public void updateNoOfChildrenCount(NoOfChildren noOfChildren, NoOfChildren noOfChildren1) {
    }

    public void updateUserCount() {
        totalUsersCount += 1;
    }

    public void updateScoreCount(int courseNo, int postAssessmentScore, int preAssessmentScore) {
        totalPreAssessmentScoreCount += preAssessmentScore;
        totalAssessmentScoreCount += postAssessmentScore;
        totalCourseCount += 1;

        if (ValidatorUtil.isCoursePasses(postAssessmentScore))
            totalCoursePassedCount += 1;
        else
            totalCourseFailedCount += 1;

        switch (courseNo){
            case 1:
                totalCourse1Count += 1;
                break;
            case 2:
                totalCourse2Count += 1;
                break;
            case 3:
                totalCourse3Count += 1;
                break;
            case 4:
                totalCourse4Count += 1;
                break;
            case 5:
                totalCourse5Count += 1;
                break;
            case 6:
                totalCourse6Count += 1;
                break;
        }
    }

    public void updateQuizCount(int score) {
        totalQuizCount += 1;
        totalQuizScoreCount += score;
    }

    public void updateFeedbackCount() {
        totalFeedbackCount += 1;
    }
}