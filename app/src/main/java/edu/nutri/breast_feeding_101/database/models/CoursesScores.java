package edu.nutri.breast_feeding_101.database.models;

/**
 * Created by ribads on 5/31/18.
 */
public class CoursesScores extends BaseModel{

    public void CourseScores(){
    }

    public static String tableName = "CourseScores";
    private Long courseNo;
    private Long preAssessmentScore;
    private Long postAssessmentScore;
    private Status status;


    public void setCourseNo(int courseNo){
        this.courseNo = Long.valueOf(courseNo);
    }

    public int getCourseNo(){
        return courseNo.intValue();
    }

    public void setPreAssessmentScore(int preAssessmentScore){
        this.preAssessmentScore = Long.valueOf(preAssessmentScore);
    }

    public int getPreAssessmentScore(){
        return preAssessmentScore.intValue();
    }

    public void setPostAssessmentScore(int postAssessmentScore){
        this.postAssessmentScore = Long.valueOf(postAssessmentScore);
    }

    public int getPostAssessmentScore(){
        return postAssessmentScore.intValue();
    }

    public void setStatus(Status status){
        this.status = status;
    }

    public Status getStatus(){
        return status;
    }

    public enum Status{
        PASS,FAILED
    }
}