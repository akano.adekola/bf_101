package edu.nutri.breast_feeding_101.database.models;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by ribads on 6/24/18.
 */

@IgnoreExtraProperties
public class ChatMessage extends BaseModel{

    public ChatMessage(){
    }

    public static String tableName = "Chats";
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
