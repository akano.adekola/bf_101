package edu.nutri.breast_feeding_101.database.models;

import com.google.firebase.database.IgnoreExtraProperties;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ribads on 4/11/18.
 */
@IgnoreExtraProperties
public class User {

    public void user(){

    }
    public static String tableName = "Users";
    private String userId;
    private String firstName;
    private String lastName;
    private String email;
    private String gender;
    private String noOfChildren;
    private String levelOfEducation;
    private String maritalStatus;
    private Long dateOfBirth;
    private Long createdDate;
    private String religion;
    private String pregnancyStatus;
    private boolean admin;

    public void setUserId(String userId){
        this.userId = userId;
    }

    public String getUserId(){
        return userId;
    }

//    public void setUserName(String userName){
//        USERNAME = userName;
//    }
//
//    public String getUserName(){
//        return USERNAME;
//    }

    public void setAdmin(boolean isAdmin){
        admin = isAdmin;
    }

    public boolean isAdmin(){
        return admin;
    }

    public void setFirstName(String firstName){
        this.firstName = firstName.trim();
    }

    public String getFirstName(){
        return firstName;
    }

    public void setLastName(String lastName){
        this.lastName = lastName.trim();
    }

    public String getLastName(){
        return lastName;
    }

    public void setEmail(String email){
        this.email = email.trim();
    }

    public String getEmail(){
        return email;
    }

    public void setGender(String gender){
        this.gender = gender;
    }

    public String getGender(){
        return gender;
    }

    public void setNoOfChildren(String noOfChildren){
        this.noOfChildren = noOfChildren;
    }

    public String getNoOfChildren(){
        return noOfChildren;
    }

    public void setLevelOfEducation(String levelOfEducation){
        this.levelOfEducation = levelOfEducation;
    }

    public String getLevelOfEducation(){
        return levelOfEducation;
    }

    public void setMaritalStatus(String maritalStatus){
        this.maritalStatus = maritalStatus;
    }

    public String getMaritalStatus(){
        return maritalStatus;
    }

    public String getPregnancyStatus() {
        return pregnancyStatus;
    }

    public void setPregnancyStatus(String pregnancyStatus) {
        this.pregnancyStatus = pregnancyStatus;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public void setDateOfBirth(Long  dateOfBirth){
        this.dateOfBirth = dateOfBirth;
    }

    public Long getDateOfBirth(){
        return dateOfBirth;
    }

    public String getDateOfBirth(String format){
        if (dateOfBirth == null)
            return "";
        Date date = new Date(dateOfBirth);
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    public String getCreatedDate(String format){
        if (createdDate == null)
            return "";
        Date date = new Date(createdDate);
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

//    @Exclude
//    public Map<String, Object> toMap() {
//        HashMap<String, Object> result = new HashMap<>();
//        result.put("userId", userId);
//        result.put("userName", USERNAME);
//        result.put("email", email);
//
//        return result;
//    }

}
