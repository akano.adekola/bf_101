package edu.nutri.breast_feeding_101.database.models;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by ribads on 6/20/18.
 */
@IgnoreExtraProperties
public class Discussions extends BaseModel{

    public Discussions(){
    }

    public static String tableName = "Discussions";
    private String title;
    private Long likes;
    private Long comments;

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

    public Long getLikes() {
        return likes;
    }

    public void setLikes(Long likes) {
        this.likes = likes;
    }

    public Long getComments() {
        return comments;
    }

    public void setComments(Long comments) {
        this.comments = comments;
    }

}
