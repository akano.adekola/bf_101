package edu.nutri.breast_feeding_101.database.models;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by ribads on 5/31/18.
 */
@IgnoreExtraProperties
public class CourseStatistics extends BaseModel{

    public void CourseStatistics(){

    }

    private String userId;
    private String firstName;
    private String lastName;
    private String email;
    private Long courseNo;
    private Long attempts;
    private Long success;
    private Long failed;
    private Long totalScore;
    private Long averageScore;

    public void setUserDetails(User user){
        userId = user.getUserId();
        email = user.getEmail();
        firstName = user.getFirstName();
        lastName = user.getLastName();
    }

    public User getUserDetails(){
        User user = new User();
        user.setUserId(userId);
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    public void setCourseNo(Long courseNo){
        this.courseNo = courseNo;
    }

    public long getCourseNo(){
        return courseNo;
    }

    public void setNoOfAttempts(Long noOfAttempts){
        attempts = noOfAttempts;
    }

    public long getNoOfAttempts(){
        return attempts;
    }

    public void setNoOfSuccess(Long noOfSuccess){
        success = noOfSuccess;
    }

    public long getNoOfSuccess(){
        return success;
    }
    public void setNoOfFailed(Long noOfFailed){
        failed = noOfFailed;
    }

    public long getNoOfFailed(){
        return failed;
    }
    public void setTotalScore(Long totalScore){
        this.totalScore = totalScore;
    }

    public long getTotalScore(){
        return totalScore;
    }
    public void setAverageScore(Long averageScore){
        this.averageScore = averageScore;
    }

    public long getAverageScore(){
        return averageScore;
    }

}
