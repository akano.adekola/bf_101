package edu.nutri.breast_feeding_101.database.models;

/**
 * Created by ribads on 6/25/18.
 */
public class FeedBack extends BaseModel{

    public void FeedBack(){
    }

    public static String tableName = "feedBack";
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
