package edu.nutri.breast_feeding_101.auth.presenter;

import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.util.ValidatorUtil;
import edu.nutri.breast_feeding_101.common.util.ValueUtil;

/**
 * Created by Akano on 1/20/2018.
 */

public class SignUpPresenter{

    private EditText firstNameEt, lastNameEt, emailEt, passwordEt;
    private TextInputLayout passwordLay;
    private TextView loginTv, forgotPasswordTv;
    private Button signUpBtn;

    public SignUpPresenter(View view) {

        firstNameEt = view.findViewById(R.id.firstNameEt);
//        firstNameEt.setText("name");
        lastNameEt = view.findViewById(R.id.lasttNameEt);
//        lastNameEt.setText("last name");
        emailEt = view.findViewById(R.id.emailEt);
//        emailEt.setText("kolaak47@yahoo.com");
        passwordEt = view.findViewById(R.id.passwordEt);
//        passwordEt.setText("muideen");
        loginTv = view.findViewById(R.id.loginTv);
        forgotPasswordTv = view.findViewById(R.id.forgot_password);
        signUpBtn = view.findViewById(R.id.signUpBtn);
        passwordLay = view.findViewById(R.id.passwordLay);
        passwordLay.setPasswordVisibilityToggleEnabled(true);

    }

    public boolean isValid() {
        return ValidatorUtil.isNameValid(firstNameEt) && ValidatorUtil.isEmailValid(emailEt) && ValidatorUtil.isPasswordValid(passwordEt);
    }

    public TextView getLoginTv() {
        return loginTv;
    }

    public Button getSignUpButton() {
        return signUpBtn;
    }

    public TextView getForgotPasswordTv() {
        return forgotPasswordTv;
    }

    public String getFirstNamee() {
        return ValueUtil.getText(firstNameEt);
    }

    public String getPassword() {
        return ValueUtil.getText(passwordEt);
    }

    public String getEmail() {
        return ValueUtil.getText(emailEt);
    }

    public String getLastName() {
        return ValueUtil.getText(lastNameEt);
    }
}