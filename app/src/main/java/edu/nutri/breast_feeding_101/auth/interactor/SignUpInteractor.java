package edu.nutri.breast_feeding_101.auth.interactor;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

import java.util.Calendar;

import edu.nutri.breast_feeding_101.auth.presenter.SignUpPresenter;
import edu.nutri.breast_feeding_101.common.util.NotificationUtil;
import edu.nutri.breast_feeding_101.common.util.ValueUtil;
import edu.nutri.breast_feeding_101.database.models.Statistics;
import edu.nutri.breast_feeding_101.database.models.User;

/**
 * Created by Akano on 1/20/2018.
 */
public class SignUpInteractor {

    private SignUpPresenter signUpPresenter;
    private OnCompleteListener onCompleteListener;
    private String firstName;
    private String lastName;

    public SignUpInteractor(View view, OnCompleteListener onCompleteListener) {
        signUpPresenter = new SignUpPresenter(view);
        this.onCompleteListener = onCompleteListener;
    }

    public void signUp(FirebaseAuth firebaseAuth){
        firstName = signUpPresenter.getFirstNamee();
        lastName = signUpPresenter.getLastName();
        String email = signUpPresenter.getEmail();
        String password = signUpPresenter.getPassword();

        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(onCompleteListener);
    }

    public boolean isValid() {
        return signUpPresenter.isValid();
    }

    public Button getSignUpButton() {
        return signUpPresenter.getSignUpButton();
    }

    public TextView getLoginTv() {
        return  signUpPresenter.getLoginTv();
    }

    public TextView getForgotPasswordTv() {
        return signUpPresenter.getForgotPasswordTv();
    }

    public void saveUsersData(FirebaseUser firebaseUser, DatabaseReference usersDatabaseReference) {

        String email = firebaseUser.getEmail();
        String userid = firebaseUser.getUid();

        User user = new User();
        user.setUserId(userid);
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setCreatedDate(ValueUtil.getEpoch(Calendar.getInstance()));

        usersDatabaseReference.child(userid).setValue(user);
    }

    public void updateUserCount(DatabaseReference baseDatabaseReference) {
        baseDatabaseReference.child(Statistics.table_name).runTransaction(new Transaction.Handler() {
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                Statistics statistics = mutableData.getValue(Statistics.class);
                statistics.updateUserCount();
                mutableData.setValue(statistics);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {

            }
        });
    }
//    public void add_to_database(FirebaseUser user) {
//
//        String user_id = user.getUid();
//        String email = user.getEmail();
//        String username = signUpPresenter.getFirstNamee();
//
//        Database DBase = new Database(context);
//
//        ContentValues values = new ContentValues();
//
//        values.put("username", username);
//        values.put("email", email);
//        values.put("user_id", user_id);
//
//        DBase.getWritableDatabase().insert(DBase.login_database, null, values);
//
//        Intent it = new Intent(context, MainActivity.class);
//        context.startActivity(it);
//
//    }
}
