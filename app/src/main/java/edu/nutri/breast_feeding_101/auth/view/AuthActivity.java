package edu.nutri.breast_feeding_101.auth.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestBatch;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.auth.fragment.LoginFragment;
import edu.nutri.breast_feeding_101.common.util.LauncherUtil;
import edu.nutri.breast_feeding_101.common.util.NotificationUtil;
import edu.nutri.breast_feeding_101.common.view.BaseActivity;
import edu.nutri.breast_feeding_101.dashboard.view.MainActivity;
import edu.nutri.breast_feeding_101.database.models.User;

public class AuthActivity extends BaseActivity {

    FirebaseAuth mAuth;
    GoogleSignInClient mGoogleSignInClient;
    int GOOGLE_SIGN_IN = 233;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        mAuth = FirebaseAuth.getInstance();
        setDefaultFragment();
        initGoogleAuth();
    }

    private void initGoogleAuth() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void setDefaultFragment() {
        LauncherUtil.launchFragment(getSupportFragmentManager(), R.id.frame, new LoginFragment(), false);
    }

    public void onGoogleClick(View view) {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
    }

    public void onFacebookClick(View view) {
        callbackManager = CallbackManager.Factory.create();
        LoginManager loginManager = LoginManager.getInstance();
        String[] permissions = {"email", "public_profile"};
        loginManager.logInWithReadPermissions(this, Arrays.asList(permissions));
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GOOGLE_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            GoogleSignInAccount account = null;
            try {
                account = task.getResult(ApiException.class);
            } catch (ApiException e) {
                e.printStackTrace();
            }

            if (account !=null)
                handleGoogleSignInResult(account);
            else
                NotificationUtil.showToast(getApplicationContext(), "Account Not Found");
        }
        else
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleGoogleSignInResult(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser firebaseUser = mAuth.getCurrentUser();

                        String email = firebaseUser.getEmail();
                        String lastName = acct.getFamilyName();
                        String firstName = acct.getGivenName();

                        saveUserData(firebaseUser, email, firstName, lastName);
                    }
                });
    }

    private void handleFacebookAccessToken(AccessToken token) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser firebaseUser = mAuth.getCurrentUser();
                        Profile currentProfile = Profile.getCurrentProfile();

                        String email = firebaseUser.getEmail();
                        String lastName = currentProfile.getLastName();
                        String firstName = currentProfile.getFirstName();

                        saveUserData(firebaseUser, email, firstName, lastName);
                    }
                });
    }

    private void saveUserData(FirebaseUser firebaseUser, String email, String firstName, String lastName) {
        String userId = firebaseUser.getUid();

        User user = new User();
        user.setEmail(email);
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setUserId(userId);
        usersDatabaseReference.child(userId).setValue(user);
        LauncherUtil.launchClass(getApplicationContext(), MainActivity.class, null, true);
    }

//    private void facebook() {
//        //        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
//
//        callbackManager = CallbackManager.Factory.create();
//
//        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
//        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
////                NotificationUtil.showToast(getApplicationContext(), Profile.getCurrentProfile().getFirstName());
////                NotificationUtil.showToast(getApplicationContext(), FirebaseAuth.getInstance().getCurrentUser() == null ? "No usrr" : FirebaseAuth.getInstance().getCurrentUser().getEmail());
//
//                Bundle parameters = new Bundle();
//                parameters.putString("fields", "id,first_name,last_name,email,birthday");
//
//                GraphRequest profileGraphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), null);
//                profileGraphRequest.setParameters(parameters);
//
//                Bundle params = new Bundle();
//                params.putBoolean("redirect", false);
//                GraphRequestBatch batchRequest = new GraphRequestBatch(profileGraphRequest);
//
//                List<GraphResponse> responses = batchRequest.executeAndWait();
//                GraphResponse profileResponse = responses.get(0);
//                JSONObject resultJSON = profileResponse.getJSONObject();
//                if (resultJSON != null) {
//                    try {
//                        String firstName = resultJSON.getString("first_name");
//                        String lastName = resultJSON.getString("last_name");
//                        String email = resultJSON.getString("email");
//                        String birthDay = resultJSON.getString("birthday");
//
//                        Log.e("ffff", firstName);
//                        Log.e("ffff", lastName);
//                        Log.e("ffff", email);
//                        Log.e("ffff", birthDay);
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//            }
//
//            @Override
//            public void onCancel() {
//                // App code
//            }
//
//            @Override
//            public void onError(FacebookException exception) {
//                // App code
//            }
//        });
//
////        callbackManager = CallbackManager.Factory.create();
////        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
////                    @Override
////                    public void onSuccess(LoginResult loginResult) {
////                        // App code
////                    }
////
////                    @Override
////                    public void onCancel() {
////                        // App code
////                    }
////
////                    @Override
////                    public void onError(FacebookException exception) {
////                        // App code
////                    }
////                });
//
////        AccessToken.getCurrentAccessToken();
////        Profile.getCurrentProfile();
//    }


}