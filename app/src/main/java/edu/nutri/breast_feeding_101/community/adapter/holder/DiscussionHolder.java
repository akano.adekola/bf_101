package edu.nutri.breast_feeding_101.community.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.Calendar;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.util.Constants;
import edu.nutri.breast_feeding_101.common.util.ValueUtil;
import edu.nutri.breast_feeding_101.dashboard.GlideApp;
import edu.nutri.breast_feeding_101.database.models.Discussions;

/**
 * Created by ribads on 6/20/18.
 */
public class DiscussionHolder extends RecyclerView.ViewHolder {
    private TextView titleTv, detailsTv, commentTv, likeTv;
    private ImageView profileImg;
    private StorageReference storageReference;

    public DiscussionHolder(View itemView) {
        super(itemView);
        titleTv = itemView.findViewById(R.id.title_tv);
        detailsTv = itemView.findViewById(R.id.details_tv);
        commentTv = itemView.findViewById(R.id.comment_tv);
        likeTv = itemView.findViewById(R.id.like_tv);
        profileImg = itemView.findViewById(R.id.profile_img);
    }

    public void bindData(Discussions discussions) {
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String title = discussions.getTitle();
        String date = ValueUtil.getDateFromEpoch(discussions.getCreatedDate());
        String creatorId = discussions.getCreatorId();
        String creatorName = creatorId.equals(userId) ? "You" : discussions.getCreatorName();
        Long comments = discussions.getComments();
        Long likes = discussions.getLikes();

        commentTv.setText(comments+"");
        likeTv.setText(likes+"");
        titleTv.setText(title);
        detailsTv.setText(itemView.getContext().getResources().getString(R.string.discussion_details, creatorName, date));
        storageReference = FirebaseStorage.getInstance().getReference().child(Constants.PROFILE_IMAGE).child(creatorId);
        showImage();
    }

    private void showImage() {
        GlideApp.with(itemView.getContext())
                .load(storageReference)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.account_circle_black)
                .placeholder(R.drawable.account_circle_black)
                .into(profileImg);
    }
}