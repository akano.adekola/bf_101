package edu.nutri.breast_feeding_101.community.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.util.Constants;
import edu.nutri.breast_feeding_101.common.util.IdGenerator;
import edu.nutri.breast_feeding_101.common.util.NotificationUtil;
import edu.nutri.breast_feeding_101.common.util.ValueUtil;
import edu.nutri.breast_feeding_101.common.view.BaseActivity;
import edu.nutri.breast_feeding_101.community.adapter.ChatAdapter;
import edu.nutri.breast_feeding_101.database.models.ChatMessage;
import edu.nutri.breast_feeding_101.database.models.User;

public class ChatActivity extends BaseActivity {

    EditText messageEt;
    RecyclerView chatMessagesRecycler;
    List<ChatMessage> chatMessages;
    ChatAdapter chatAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat, "Discussion Room");
        String id = Objects.requireNonNull(getIntent().getExtras()).getString("id");
        databaseReference = databaseReference.child(ChatMessage.tableName).child(id);
        initView();
    }

    private void initView() {
        messageEt = findViewById(R.id.message_et);
        chatAdapter = new ChatAdapter();
        chatMessagesRecycler = findViewById(R.id.message_recycler);
        chatMessagesRecycler.setLayoutManager(new LinearLayoutManager(this));
        chatMessagesRecycler.setAdapter(chatAdapter);
        getChatMessages();
    }

    private void getChatMessages() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshots) {
                chatMessages = new ArrayList<>();
                for (DataSnapshot dataSnapshot : dataSnapshots.getChildren()){
                    chatMessages.add(dataSnapshot.getValue(ChatMessage.class));
                }
                chatAdapter.updateList(chatMessages);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public void sendMessage(View view) {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setId(new IdGenerator().getChatMessageId());
        chatMessage.setCreatorId(user.getUserId());
        chatMessage.setCreatorName(user.getFirstName());
        chatMessage.setMessage(ValueUtil.getText(messageEt));
        databaseReference.push().setValue(chatMessage);
        ValueUtil.clearText(messageEt);
    }

    public void attachImage(View view) {
        CropImage.activity()
                .setAspectRatio(1, 1)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                uploadImage(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void uploadImage(Uri resultUri) {
        ChatMessage chatMessage = new ChatMessage();
        String id = new IdGenerator().getChatMessageId();
        chatMessage.setId(id);
        chatMessage.setCreatorId(user.getUserId());
        chatMessage.setCreatorName(user.getFirstName());
        chatMessage.setMessage(Constants.IMAGE_CODE);

        showProgress("uploading.....");
        StorageReference chatImageReference = FirebaseStorage.getInstance().getReference().child(Constants.CHAT_IMAGE).child(id);
        UploadTask uploadTask = chatImageReference.putFile(resultUri);
        uploadTask.addOnFailureListener(exception -> {
            hideProgress();
        })
        .addOnSuccessListener(taskSnapshot -> {
            hideProgress();
            databaseReference.push().setValue(chatMessage);
        })
        .addOnProgressListener(taskSnapshot -> {
            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
            hideProgress();
        });
    }
}
