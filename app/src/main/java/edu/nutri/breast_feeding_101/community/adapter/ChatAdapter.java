package edu.nutri.breast_feeding_101.community.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.community.adapter.holder.ChatHolder;
import edu.nutri.breast_feeding_101.database.models.ChatMessage;

/**
 * Created by ribads on 6/20/18.
 */
public class ChatAdapter extends RecyclerView.Adapter<ChatHolder>{

    private List<ChatMessage> chatMessages = new ArrayList<>();
    @NonNull
    @Override
    public ChatHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType == 1 ? R.layout.holder_chat2 : R.layout.holder_chat, parent, false);
        return new ChatHolder(view);
    }

    private boolean isChatMine(int position) {
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        return chatMessages.get(position).getCreatorId().equals(userId);
    }

    @Override
    public int getItemViewType(int position) {
        if (isChatMine(position))
            return 1;
        else
            return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull ChatHolder holder, int position) {
        holder.bindData(chatMessages.get(position));
    }

    @Override
    public int getItemCount() {
        return chatMessages.size();
    }

    public void updateList(List<ChatMessage> chatMessages){
        this.chatMessages = chatMessages;
        notifyDataSetChanged();
    }
}
