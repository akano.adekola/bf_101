package edu.nutri.breast_feeding_101.community.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.util.Constants;
import edu.nutri.breast_feeding_101.common.util.ValueUtil;
import edu.nutri.breast_feeding_101.dashboard.GlideApp;
import edu.nutri.breast_feeding_101.database.models.ChatMessage;

/**
 * Created by ribads on 6/24/18.
 */
public class ChatHolder extends RecyclerView.ViewHolder {
    private TextView messageTv, detailsTv;
    private ImageView profileImg, chatImg;
    private StorageReference profileImageReference, chatImageReference;

    public ChatHolder(View itemView) {
        super(itemView);
        messageTv = itemView.findViewById(R.id.message_tv);
        detailsTv = itemView.findViewById(R.id.details_tv);
        profileImg = itemView.findViewById(R.id.profile_img);
        chatImg = itemView.findViewById(R.id.chat_img);
    }

    public void bindData(ChatMessage chatMessage) {
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String message = chatMessage.getMessage();
        checkForImage(message);
        String date = ValueUtil.getDateFromEpoch(chatMessage.getCreatedDate());
        String creatorId = chatMessage.getCreatorId();
        String CreatorName = creatorId.equals(userId) ? "You" : chatMessage.getCreatorName();

        messageTv.setText(message);
        detailsTv.setText(itemView.getResources().getString(R.string.discussion_details, CreatorName, date));

        StorageReference baseStorageReference = FirebaseStorage.getInstance().getReference();
        profileImageReference = baseStorageReference.child(Constants.PROFILE_IMAGE).child(creatorId);
        chatImageReference = baseStorageReference.child(Constants.CHAT_IMAGE).child(chatMessage.getId());

        showImage();
    }

    private void checkForImage(String message) {
        if (message.equals(Constants.IMAGE_CODE)){
            showChatImage();
            messageTv.setVisibility(View.GONE);
        }
        else
            chatImg.setVisibility(View.GONE);

    }

    private void showChatImage() {
        GlideApp.with(itemView.getContext())
                .load(chatImageReference)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.account_circle_black)
                .placeholder(R.drawable.account_circle_black)
                .into(chatImg);
    }

    private void showImage() {
        GlideApp.with(itemView.getContext())
                .load(profileImageReference)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.account_circle_black)
                .placeholder(R.drawable.account_circle_black)
                .into(profileImg);
    }
}
