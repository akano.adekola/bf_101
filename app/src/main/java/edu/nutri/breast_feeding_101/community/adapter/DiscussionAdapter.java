package edu.nutri.breast_feeding_101.community.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.listener.OnItemClickListener;
import edu.nutri.breast_feeding_101.community.adapter.holder.DiscussionHolder;
import edu.nutri.breast_feeding_101.database.models.Discussions;

/**
 * Created by ribads on 6/20/18.
 */
public class DiscussionAdapter extends RecyclerView.Adapter<DiscussionHolder>{

    private OnItemClickListener<String> onItemClickListener;
    private List<Discussions> discussionsList = new ArrayList<>();

    @NonNull
    @Override
    public DiscussionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_disscussion, parent, false);
        return new DiscussionHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DiscussionHolder holder, int position) {
        Discussions discussions = discussionsList.get(position);
        holder.itemView.setOnClickListener(v -> onItemClickListener.onItemClick(discussions.getId()));
        holder.bindData(discussions);
    }

    @Override
    public int getItemCount() {
        return discussionsList.size();
    }

    public void attachListener(OnItemClickListener<String> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void updateList(List<Discussions> discussionsList) {
        this.discussionsList = discussionsList;
        notifyDataSetChanged();
    }

}