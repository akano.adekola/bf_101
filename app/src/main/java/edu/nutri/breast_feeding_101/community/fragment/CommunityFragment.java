package edu.nutri.breast_feeding_101.community.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.nutri.breast_feeding_101.R;
import edu.nutri.breast_feeding_101.common.listener.OnItemClickListener;
import edu.nutri.breast_feeding_101.common.util.IdGenerator;
import edu.nutri.breast_feeding_101.common.util.NotificationUtil;
import edu.nutri.breast_feeding_101.common.util.ValidatorUtil;
import edu.nutri.breast_feeding_101.common.util.ValueUtil;
import edu.nutri.breast_feeding_101.common.view.BaseFragment;
import edu.nutri.breast_feeding_101.community.adapter.DiscussionAdapter;
import edu.nutri.breast_feeding_101.community.view.ChatActivity;
import edu.nutri.breast_feeding_101.database.models.Discussions;
import edu.nutri.breast_feeding_101.database.models.Statistics;
import edu.nutri.breast_feeding_101.database.models.User;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class CommunityFragment extends BaseFragment implements OnItemClickListener<String> {

    EditText discussionEt;
    RecyclerView discussionRecycler;
    DiscussionAdapter discussionAdapter;
    List<Discussions> discussionsList;
    DatabaseReference databaseReference;
    public CommunityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return getFragmentView(inflater, R.layout.fragment_community, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view) {
        databaseReference = baseDatabaseReference.child(Discussions.tableName);
        discussionEt = view.findViewById(R.id.discussion_et);
        discussionRecycler = view.findViewById(R.id.discussion_recycler);
        view.findViewById(R.id.send_img).setOnClickListener(v -> {
            if (ValidatorUtil.isNotEmpty(discussionEt))
                postDiscussion(ValueUtil.getText(discussionEt));
        });

        discussionAdapter = new DiscussionAdapter();
        discussionAdapter.attachListener(this);
        discussionRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        discussionRecycler.setAdapter(discussionAdapter);
        getDiscussions();

    }

    private void postDiscussion(String discussionTitle) {
        Discussions discussions = new Discussions();
        discussions.setId(new IdGenerator().getDiscussionId());
        discussions.setTitle(discussionTitle);
        discussions.setLikes(0L);
        discussions.setComments(0L);
        discussions.setCreatorId(userId);
        discussions.setCreatorName(user.getFirstName());
        databaseReference.push().setValue(discussions);
        ValueUtil.clearText(discussionEt);

        discussionCounter();
    }

    private void discussionCounter() {
//        Statistics statistics = new Statistics();
//        statistics.setDiscussionCount(1L);
//        statistics.setUsersCount(1L);
//        baseDatabaseReference.child(Statistics.table_name).setValue(statistics);
        baseDatabaseReference.child(Statistics.table_name).runTransaction(new Transaction.Handler() {
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                Statistics statistics = mutableData.getValue(Statistics.class);
                statistics.updateDiscussionCount();
                mutableData.setValue(statistics);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {
//                NotificationUtil.showToast(getActivity(), "Done discussion counter");
            }
        });
    }

    private void getDiscussions() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshots) {
                discussionsList = new ArrayList<>();
                for (DataSnapshot dataSnapshot : dataSnapshots.getChildren()){
                    Discussions discussions = dataSnapshot.getValue(Discussions.class);
                    discussionsList.add(discussions);
                }
                Collections.reverse(discussionsList);
                discussionAdapter.updateList(discussionsList);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onItemClick(String id) {
        Intent intent = new Intent(getActivity(), ChatActivity.class);
        intent.putExtra("id", id);
        startActivityForResult(intent, 2863);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK){

        }
    }
}